/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/


//========================================================================
// ~~~~~ Libraries to include ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>
#include <RH_RF95.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~
// TrollPic 1 and TollPic 2

//#define TP1                     1
#define TP2                     1

// User Changeable Variables..............................................

// Frequency of data collection to Feather RAM & writing to SD
#define DATA_TIME               100           
// Altitude limit for cutdown relay
#define CUT_OFF_ALT             30000       
// How long to leave the relay active
#define BURN_TIME               5000
// How long we will wait to until we retry the burn
#define RETRY_TIME              5000

// LoRa TX Variable Definition........................................................

// Time between sending LoRa packets (msec)
#define TX_PERIOD               5000 
// Pulse width on reset assert (msec)
#define RH_RF95_RESET_DELAY     10  
// Must match frequency of target (MHz)

#ifdef TP1
#define RF95_FREQ               433
#endif

#ifdef TP2
#define RF95_FREQ               915
#endif

// Packet length (RH_RF95_MAX_MESSAGE_LEN = 255 - RH_RF95_HEADER_LEN)
#define MAX_RADIO_PACKET_LEN    100
// Transmit attempt timeout (msec) 
#define TX_TIMEOUT              4500  
// External blink period (msec)
#define EXTLED_DELAY            100

// Pin selection stuff...................................................

#define SHUTDOWN_PIN            14
#define RELAY_PIN               18
#define GEIGER_PIN_1            15
#define GEIGER_PIN_2            16
#define GEIGER_PIN_3            17
#define EXTLED_PIN              5
#define ARDLED_PIN              8

// Pin assignments for LoRa SPI interface
#define RFM95_CS                10
#define RFM95_INT               6 
#define RFM95_RST               9

#define NUM_COUNTS              10
#define CUTDOWN_COUNT_DELAY     1000

// Debug variables ......................................................
// Turn off debug mode for data collection by commenting out this line below
//#define DEBUG_DATA            1  
// Turn off debug mode for the LoRa by commenting out this line below
//#define DEBUG_LORA            1 
// Turn off debug mode for geiger collection by commenting out this line below
//#define DEBUG_GEIGER          1

// set timers prior to running the loop...................................
uint32_t data_timer  = millis();     // On-board clock timer for data collection state
uint32_t radio_timer = 0;            // on-board clock

// GPS Variable definition................................................

//Change the name of Serial1 (the UART) to mySerial
#define mySerial Serial1 
Adafruit_GPS GPS(&mySerial);
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean);     // Func prototype keeps Arduino 0023 happy

// SD Card ..............................................................
const int chipSelect = 4;               // Chip select for SD Card
File dataFile;                          //Variable for opening, closing, writing and flushing the data file
File geigerFile;                        //Variable for opening, closing, writing and flushing the geiger file
char filename1[] = "LOGGER00.txt";      // Name of the datafile stored on the SD card for logging data in state 1.
char filename2[] = "GEIGER00.txt";      // Name of the datafile stored on the SD card for logging and time stamping geiger data.

//Adafruit ID assigning..................................................

Adafruit_BNO055 bno = Adafruit_BNO055(55);    // Assign a unique ID to the BNO055
Adafruit_ADS1115 ads1115;                     // Assign a unique ID to the ADC
Adafruit_VEML6070 uv = Adafruit_VEML6070();   // Assign a unique ID to the VEML6070

// Data Collection Variables.............................................
float tempCelsius1, tempCelsius2;             // Temperature in celsius from the thermistors on the ADC
float GUVA_B_volt;                            // Analog UV_B sensor on the ADC
float UV_A_dig;                               // Digital UV_A sensor on the I2C line
uint32_t gCounter1, gCounter2, gCounter3;     // Counter for pin 1, 2 and 3
uint32_t gCounts1, gCounts2, gCounts3;        // Total counts over an interval for pin 1, 2 and 3
uint32_t gDataArray[100][2];                  // Column1 = pin#, column2 = time in millis
uint16_t gArrayCtr = 0;                       // Will keep track of entries in the array           

//Relay Variables........................................................
int  relayBurned     = 0; //Relay burn counter
int  delayRelay      = 0;
int  relayBurn_try   = 0; //How many times did the burn cut off occur

//LoRa Variables........................................................
bool loraForceReset  = false;
bool loraInitSuccess = false;

RH_RF95 rf95(RFM95_CS, RFM95_INT); // Singleton instance of the radio driver
int lora_init(uint8_t newMode);    // Prototype of lora reset function
char radiopacket[MAX_RADIO_PACKET_LEN] = {0}; // Initializing as char array


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  
  Serial.begin(115200);   //Set USB serial speed
  #ifdef DEBUG_DATA
  while (!Serial) delay(1);
  Serial.println("HAB Balloon Data Test"); Serial.print("");
  #endif
  
  //LoRa TX Mode Setup......................................................
  pinMode(RFM95_RST, OUTPUT);
  
  //Status LED Setup......................................................
  pinMode(EXTLED_PIN, OUTPUT);
  pinMode(ARDLED_PIN, OUTPUT);
  
  //Geiger Counts Setup...................................................
  pinMode(GEIGER_PIN_1, INPUT);
  pinMode(GEIGER_PIN_2, INPUT);
  pinMode(GEIGER_PIN_3, INPUT);
  attachInterrupt(digitalPinToInterrupt(GEIGER_PIN_1), ISR_GC1, RISING); 
  attachInterrupt(digitalPinToInterrupt(GEIGER_PIN_2), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GEIGER_PIN_3), ISR_GC3, FALLING);
  
  //Cutdown Relay Setup...................................................
  
  pinMode(RELAY_PIN,OUTPUT);
  
  //Shutdown Setup........................................................
  
  pinMode(SHUTDOWN_PIN,INPUT); 
  digitalWrite(SHUTDOWN_PIN,HIGH);

  //GPS Setup.............................................................

  // Setting the speed of the GPS serial bus
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);    // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA);               // Request updates on antenna status, comment out to keep quiet
  delay(1000);                                  // Think it allows 1 update from the GPS first.
  
  //SD CARD SETUP.........................................................

  #ifdef DEBUG_DATA
    Serial.println("Initializing SD card...");
    // make sure that the default chip select pin is set to
    // output, even if you don't use it
  #endif
  
  pinMode(SS, OUTPUT);
  SD.begin(chipSelect);

    // create a new file
  for (uint8_t i = 0; i < 100; i++) {
    filename1[6] = i/10 + '0';
    filename1[7] = i%10 + '0';
    if (! SD.exists(filename1)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename1, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String dataheader = "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";
  dataFile.println(dataheader);
  dataFile.flush(); //Flush data and write to SD card
  dataFile.close(); //Close the current data file

  for (uint8_t i = 0; i < 100; i++) {
    filename2[6] = i/10 + '0';
    filename2[7] = i%10 + '0';
    if (! SD.exists(filename2)) {
      // only open a new file if it doesn't exist
      geigerFile = SD.open(filename2, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String geigerheader = "Geiger Pin, Clock Time";
  geigerFile.println(geigerheader);
  geigerFile.flush(); //Flush data and write to SD card
  geigerFile.close(); //Close the current data file
  
  //BNO Setup.........................................................
  bno.begin();
  bno.setExtCrystalUse(true);

  //ADC (ADS115) Setup.........................................................
  ads1115.begin();
  ads1115.setGain(GAIN_TWOTHIRDS); // Set ADC input gain to 2/3 so that range is from 0V to 6.144V

  //Serial UV Setup.........................................................
  uv.begin(VEML6070_HALF_T);  // pass in the integration time constant

  //Pre-Flight Readouts.................................................................
  #ifdef DEBUG_DATA
    Serial.println(filename1);
    Serial.println(dataheader);
  #endif
  
  /* Add in pre-flight readouts such as cut-off altitude etc.*/

  

}



// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {


  static float currentdata[25] = {0};

  // GPS parsing for data collection...................................

  // in case you are not using the interrupt above, you'll
  // need to 'hand query' the GPS, not suggested :(
  if (! usingInterrupt) {
    // read data from the GPS in the 'main loop'
    char c = GPS.read();
    // if you want to debug, this is a good time to do it!
  }

  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived()) {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences! 
    // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
    //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
  
    if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }


  // ~~~~~ EXTERNAL LED BLINKING ~~~~~
  //========================================================================
  static bool     toggle   = false;
  static uint32_t timerLED = 0;
  if (millis() - timerLED > EXTLED_DELAY )
  {
    toggle = !toggle;
    timerLED = millis();
    digitalWrite(ARDLED_PIN, toggle); 
    digitalWrite(EXTLED_PIN, toggle);
    
  }


  // ~~~~~ BEGIN LORA MODE SWITCHING STATE ~~~~~
  //========================================================================
  static uint8_t lora_mode = RH_RF95_LORA_MODE_DEFAULT;
  static uint8_t radio_packet_len = 0;
  
  
  if (GPS.minute % 10 >= 0 && GPS.minute % 10 < 3)
    lora_mode = RH_RF95_LORA_MODE_1; // set LoRa mode 1 when minutes is 0<=t<3
   

  else if (GPS.minute % 10 >= 3 && GPS.minute % 10 < 6) 
    lora_mode = RH_RF95_LORA_MODE_2; // set LoRa mode 2 when minutes is 3<=t<6


  else if (GPS.minute % 10 >= 6 && GPS.minute % 10 <= 9)
    lora_mode = RH_RF95_LORA_MODE_3; // set LoRa mode 3 when minutes is 6<=t<9


  #ifdef DEBUG_LORA 
  static int printDelay = 0;
  if(millis() - printDelay > 10000){
    printDelay = millis();
    Serial.print("\n\rmode = "); 
    Serial.println(lora_mode);
    Serial.print("minute = ");
    Serial.print(GPS.minute);
    Serial.print(",");
    Serial.print(GPS.minute%10);
  }
  #endif
    
  // ~~~~~ BEGIN DATA COLLECTION & WRITING STATE ~~~~~
  //========================================================================

  if (millis() - data_timer >= DATA_TIME ) { 
    data_timer = millis(); // reset the timer
 
    
    //Geiger Data Collection.....................................................
    
    gCounts1 = gCounter1;
    gCounts2 = gCounter2;
    gCounts3 = gCounter3;
    gCounter1 = 0;
    gCounter2 = 0;
    gCounter3 = 0;
    
    //BNO Data Collection........................................................

    //Getting Euler variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER); 
    
    //Getting Rotation Rate variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); 
        
    int8_t bno_temp = bno.getTemp();
    
    //ADC Data Collection (temp and UV sensors) (**needs changing)........................................
    #ifdef TP1
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    float tempVolt1 = ads1115.readADC_SingleEnded(0)*6.144/32768;         // digital representation of the voltage from one of the temperature sensors
    tempCelsius1 = (tempVolt1 - 0.5)/0.01;
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    float tempVolt2 = ads1115.readADC_SingleEnded(1)*6.144/32768;         // digital representation of the voltage from one of the temperature sensors
    tempCelsius2 = (tempVolt2 - 0.5)/0.01;
    delay(10);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    GUVA_B_volt = ads1115.readADC_SingleEnded(2)*6.144/32768;          // digital representation of the voltage from the GUVA (analog UV sensor)
    #endif

    #ifdef TP2
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    float tempVolt1 = ads1115.readADC_SingleEnded(0)*6.144/32768;         // digital representation of the voltage from one of the temperature sensors
    tempCelsius1 = (tempVolt1 - 0.5)/0.01;
    delay(2); 
    float tempVolt2 = 0;
    delay(10);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    GUVA_B_volt = ads1115.readADC_SingleEnded(1)*6.144/32768;          // digital representation of the voltage from the GUVA (analog UV sensor)
    #endif
    
    UV_A_dig = uv.readUV();                             // digital representation of the voltage from the UVA
    // Data collection for print..................................................
    // String header: "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA_B_volt, UV_A_dig, gCounts1, gCounts2, gCount3, relayBurn_try, bno_temp, tempCelcius1, tempCelcius2, FREQ";

    // Assign collected data to current data array
    currentdata[0]  = millis();
    currentdata[1]  = GPS.hour;
    currentdata[2]  = GPS.minute;
    currentdata[3]  = GPS.seconds;
    currentdata[4]  = GPS.milliseconds;
    currentdata[5]  = GPS.fix;
    currentdata[6]  = GPS.latitude;
    currentdata[7]  = GPS.longitude;
    currentdata[8]  = GPS.altitude;
    currentdata[9]  = euler.x();
    currentdata[10] = euler.y();
    currentdata[11] = euler.z();
    currentdata[12] = gyro.x();
    currentdata[13] = gyro.y();
    currentdata[14] = gyro.z();
    currentdata[15] =  GUVA_B_volt;
    currentdata[16] =  UV_A_dig;
    currentdata[17] = gCounts1;
    currentdata[18] = gCounts2;
    currentdata[19] = gCounts3;
    currentdata[20] = relayBurn_try;
    currentdata[21] = bno_temp;
    currentdata[22] = tempCelsius1;
    currentdata[23] = tempCelsius2;
    currentdata[24] = RF95_FREQ;


    //Write to SD Card if the shutdown pin isn't low........................................................
    if (digitalRead(SHUTDOWN_PIN) == HIGH) {
      dataFile = SD.open(filename1, FILE_WRITE);
      for (int X=0; X<=24; X++){
        #ifdef DEBUG_DATA
        Serial.print(currentdata[X]);
        #endif
        dataFile.print(currentdata[X]);
        if (X<24) {
          #ifdef DEBUG_DATA
          Serial.print(",");
          #endif
          dataFile.print(",");
        }
      }
      #ifdef DEBUG_DATA
      Serial.println();
      #endif
      dataFile.println();
      //Flushing data and reseting counter......................................
      dataFile.flush(); //Flush data and write to SD card
      dataFile.close(); //Close the data file after
    }
    
  }

    
  // ~~~~~ BEGIN LORA PUSH STATE ~~~~~
  //========================================================================

  // LoRa TX code outside of setup and loop..................................................
  static int16_t packetnum = 0;  // packet counter, we increment per xmission

  if (millis() - radio_timer > TX_PERIOD) {
    radio_timer = millis(); 

    if( lora_init(lora_mode) == RH_RF95_ERR_NO_CHANGE) {

      #ifdef DEBUG_LORA
      Serial.print("\n\rTransmitting..."); // Send a message to rf95_server
      #endif

      // LoRa radiopacket array
      memset(radiopacket, 0, MAX_RADIO_PACKET_LEN);

      String radiopacketTemp;

      radiopacketTemp= String((uint16_t)RF95_FREQ) + "\t" +     // TX Frequency
        String((uint16_t)++packetnum) + "\t" +                    // Packet number
        String((uint8_t)currentdata[1]) + ":" +                 // Hours
        String((uint8_t)currentdata[2]) + ":" +                 // Minutes
        String(currentdata[3]+(currentdata[4]/1000)) + "\t" +   // Seconds
        String((uint8_t)currentdata[5]) + "\t" +                // GPS Fix
        String(currentdata[6]) + "\t" +                         // GPS Lattitude
        String(currentdata[7]) + "\t" +                         // GPS Longitude
        String(currentdata[8]) + "\t" +                         // Alttitude
        String(currentdata[15]) + "\t" +                        // GUVA
        String(currentdata[16]) + "\t" +                        // UV_A
        String((uint16_t)currentdata[17]) + "\t" +                        // Gieger Counter 0
        String((uint16_t)currentdata[18]) + "\t" +                        // Gieger Counter 1
        String((uint16_t)currentdata[19]) + "\t" +                        // Gieger Counter 2
        String((uint8_t)currentdata[20]);                       // Burn Attempts

      switch(lora_mode)
      {
        case RH_RF95_LORA_MODE_1:
          radio_packet_len = 75;
        break;
        case RH_RF95_LORA_MODE_2:
          radio_packet_len = 100;
        break;
        case RH_RF95_LORA_MODE_3:
          radio_packet_len = 100;
        break;
        default:
          #ifdef DEBUG_LORA
          Serial.print("\n\rERROR! Invalid mode while building radiopacket.");
          #endif
        break;
      }

      radiopacketTemp.toCharArray(radiopacket,radio_packet_len);
      
      if(!loraInitSuccess)
       loraForceReset = true;
      Serial.println("Ethan is a cool guy");
      if(!rf95.send((uint8_t*)radiopacket, radio_packet_len, 100))
      {
        loraForceReset = true; 

        #ifdef DEBUG_LORA
        Serial.print("\n\rTimed out");
        #endif
      }
    }
  }

  // ~~~~~ BEGIN GEIGER SD WRITING STATE ~~~~~
  //========================================================================

  if ( gArrayCtr > 75 && digitalRead(SHUTDOWN_PIN) == HIGH ) { 
    geigerFile = SD.open(filename2, FILE_WRITE);
    
    //Write raw Geiger data to SD card
    for (int i = 0; i <= gArrayCtr; i++) { 
      #ifdef DEBUG_GEIGER
      Serial.print(gDataArray[i][1]);
      Serial.print(",");
      Serial.print(gDataArray[i][2]);
      Serial.println();
      #endif
      geigerFile.print(gDataArray[i][1]);
      geigerFile.print(",");
      geigerFile.print(gDataArray[i][2]);
      geigerFile.println();
    }
    //Flushing data
    geigerFile.flush(); //Flush data and write to SD card
    geigerFile.close(); //Close the data file after
    
    // Clear out data array and reset counter
    gDataArray[100][2] = {0};
    gArrayCtr = 0;
    
  }
  
  // ~~~~~ BEGIN CUT DOWN RELAY STATE ~~~~~
  //========================================================================
 
  static uint8_t cutDownCntr = 0;
  static unsigned long delayCountTime = 0;


  // Cutoff relay altitude check...........................................
  if ( GPS.altitude >= CUT_OFF_ALT && relayBurned == 0) {
    if (cutDownCntr < NUM_COUNTS) {
      if (millis() - delayCountTime > CUTDOWN_COUNT_DELAY) {
        delayCountTime = millis();
        cutDownCntr++;
        
      }
    }
    else {
      // set pin high to activate relay
      digitalWrite(RELAY_PIN, HIGH); 
      delayRelay = millis();
      relayBurned = 1;
      relayBurn_try++;
    }
  }
  else{
    cutDownCntr = 0;
  }

  // Relay shutoff...........................................
  
  // Relay switch to low voltage - turns the COM to NC
  if ((millis() - delayRelay)>= RETRY_TIME+BURN_TIME && relayBurned==1){
    relayBurned = 0;
  } 
  
  else if (millis() - delayRelay >= BURN_TIME) {
    digitalWrite(RELAY_PIN, LOW); 
  }
  

} // End loop function

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

void ISR_GC1() {
  gCounter1++;
  gDataArray[gArrayCtr][1] = 1;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  if(gArrayCtr < 100)
    gArrayCtr++;            // increase array counter
}

void ISR_GC2() {
  gCounter2++;
  gDataArray[gArrayCtr][1] = 2;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  if(gArrayCtr < 100)
    gArrayCtr++;            // increase array counter  
}

void ISR_GC3() {
  gCounter3++;
  gDataArray[gArrayCtr][1] = 3;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  if(gArrayCtr < 100)
    gArrayCtr++;            // increase array counter    
}


//=================================================================================


// ~~~~~ LoRa Mode Switching Subfunction ~~~~~
//================================================================================

//  // LoRa Mode Switching Subfunction description...................................................
//    //Fill in description here
int lora_init(uint8_t newMode){

    static uint8_t          mode      = RH_RF95_LORA_MODE_INITIAL;
    static unsigned long    resetWait = 0;
    static bool             resetting = false;

    if (loraForceReset)
    {
    #ifdef DEBUG_LORA
        Serial.print("\n\rLoRa not responding! Attempting reset...");
    #endif
      loraForceReset = false;
      loraInitSuccess = false;
    }
    else if (mode == newMode)
    {
        return RH_RF95_ERR_NO_CHANGE;
    }


    if (!resetting)
    {
        digitalWrite(RFM95_RST, LOW);
        resetWait = millis();
    }

    resetting = true;

    if (millis() - resetWait < RH_RF95_RESET_DELAY)
    {
        return RH_RF95_ERR_RESET_WAIT;
    }

    digitalWrite(RFM95_RST, HIGH);

    resetting = false;

    if (!rf95.init())
    {
    #ifdef DEBUG_LORA
        Serial.print("\n\rERROR! LoRa radio init failed.");
    #endif
        return RH_RF95_ERR_LORA_INIT;
    }

    if (!rf95.setFrequency(RF95_FREQ)) {
    #ifdef DEBUG_LORA
        Serial.print("\n\rERROR! setFrequency failed.");
    #endif
        return RH_RF95_ERR_SET_FREQ;
    }

    rf95.setTxPower(20, false);

    rf95.sleep();

    if (rf95.setOPModeLoRa() != RH_RF95_ERR_NO_ERROR) {
    #ifdef DEBUG_LORA
        Serial.print("\n\rERROR! Failed to enter LoRa Mode.");
    #endif
        return RH_RF95_ERR_LORA_MODE;
    }

    rf95.setModeIdle();

    Serial.print("new mode = ");
    Serial.println(newMode);

    switch(newMode){
    case RH_RF95_LORA_MODE_DEFAULT:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    case RH_RF95_LORA_MODE_1:
      rf95.setBandWidth(RH_RF95_BW_500KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_8);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_4096CPS);
    break;
    case RH_RF95_LORA_MODE_2:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_7);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_1024CPS);
    break;
    case RH_RF95_LORA_MODE_3:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_6);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    }

    rf95.readRegModemConfig();

    mode = newMode;

    loraInitSuccess = true;

    return RH_RF95_ERR_NO_ERROR;

}


