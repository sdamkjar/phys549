/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Gardiner
 * Created: July 26, 2017
 * 
 ***************************************************************************************************/

//========================================================================
// ~~~~~ Libraries required ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

uint32_t data_time=100;         // sample rate to read data to timer
uint32_t LoRa_mode_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000;          // Time between SD write
uint32_t LoRa_push_time=5000;   // Time between LoRa push
uint32_t cutoffAlt = 3000;      // Altitude limit
uint32_t burn_time = 15000;     // How long to leave the relay active
int LoRaMode = 1;               // --> either 1, 2, or 3

// set timers prior to running the loop...................................
uint32_t state1timer = millis();  //on-board clock
uint32_t state2timer = millis();  //GPS time
uint32_t state3timer = millis();  //on-board clock
uint32_t state4timer = millis();  //on-board clock
uint32_t relayTest_time=millis(); //timer to test the relay

// GPS Variable definition................................................


// SD Card ..............................................................


// Adafruit ID assigning..................................................


// Pin selection stuff...................................................


// Data Collection Variables.............................................


// Relay Variables........................................................


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  Serial.begin(115200);      //Set USB serial speed
  Serial.print("HAB Balloon Data Test"); Serial.print("");

  //LoRa Mode Setup......................................................


  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

 
  //Cutdown Relay Setup...................................................
  
  
  //Shutdown Setup........................................................
  

  //GPS Setup.............................................................

  
  //SD CARD SETUP.........................................................

  
  //BNO Setup.........................................................


  //ADC (ADS115) Setup.........................................................


  //Serial UV Setup.........................................................

  
  //Pre-Flight Readouts.................................................................

  

}


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity








  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================

  //State 2 Description.....................................................






  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)







  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION







  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here
  




}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

  // ISR description...................................................
    //Fill in description here




//=================================================================================

