/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/


//========================================================================
// ~~~~~ Libraries to include ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

uint32_t data_time=100; // sample rate to read data to timer
uint32_t LoRa_mode_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000; // Time between SD write
uint32_t LoRa_push_time=5000; // Time between LoRa push
uint32_t cutoffAlt = 3000; //Altitude limit
uint32_t burn_time = 15000; //How long to leave the relay active
int LoRaMode = 1;             // --> either 1, 2, or 3

#define DEBUG = true //Turn on and off debug mode by setting DEBUG as "true" or "false"

// set timers prior to running the loop...................................
uint32_t state1timer = millis(); //on-board clock
uint32_t state2timer = millis(); //GPS time
uint32_t state3timer = millis(); //on-board clock
uint32_t state4timer = millis(); //on-board clock
uint32_t relayTest_time=millis(); //timer to test the relay

// GPS Variable definition................................................



// SD Card ..............................................................
const int chipSelect = 4; // Selecting chip for the SD Card
File dataFile;   // Name of the datafile stored on the SD card for logging data in state 1.
File geigerFile; // Name of the datafile stored on the SD card for logging and time stamping geiger data.
char filename1[] = "LOGGER00.txt";
char filename2[] = "GEIGER00.txt";

//Adafruit ID assigning..................................................



// Pin selection stuff...................................................
const byte GeigerPin[3] = {15, 16, 17}; // Set interrupt pin locations for Geiger counters --> pin17 is AND gate



// Data Collection Variables.............................................
uint32_t gCounter[3];             // ISR = Geiger counter logs cosmic ray events--> gCounter[3] = AND output 
uint32_t gCounts[3];              // --> used to log data at state1rate increments

//Relay Variables........................................................


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  
  #ifdef DEBUG
    Serial.begin(115200);   //Set USB serial speed
    Serial.print("HAB Balloon Data Test"); Serial.print("");
    while (!Serial) delay(1);
  #endif

  //LoRa Mode Setup......................................................

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

  pinMode(GeigerPin[0], INPUT);
  pinMode(GeigerPin[1], INPUT);
  pinMode(GeigerPin[2], INPUT);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[0]), ISR_GC1, FALLING); 
  attachInterrupt(digitalPinToInterrupt(GeigerPin[1]), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[2]), ISR_GC3, FALLING);
  
  //Cutdown Relay Setup...................................................
  

  
  //Shutdown Setup........................................................
  


  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  

  
  //SD CARD SETUP.........................................................


  #ifdef DEBUG
    Serial.print("Initializing SD card...");
    // make sure that the default chip select pin is set to
    // output, even if you don't use it:
  #endif
  
  pinMode(SS, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    #ifdef DEBUG
    Serial.println("Card failed, or not present");
    // don't do anything more:
    #endif
    while (1) ;
  }

  #ifdef DEBUG
  Serial.println("card initialized.");
  #endif
    // create a new file
    

  for (uint8_t i = 0; i < 100; i++) {
    filename1[6] = i/10 + '0';
    filename1[7] = i%10 + '0';
    if (! SD.exists(filename1)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename1, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String dataheader = "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";
  dataFile.println(dataheader);
  
  dataFile.flush(); //Flush data and write to SD card

  dataFile.close(); //Close the current data file

  for (uint8_t i = 0; i < 100; i++) {
    filename2[6] = i/10 + '0';
    filename2[7] = i%10 + '0';
    if (! SD.exists(filename2)) {
      // only open a new file if it doesn't exist
      geigerFile = SD.open(filename2, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String geigerheader = "Clock_Time, Geiger Counter";
  
  geigerFile.println(geigerheader);
  
  geigerFile.flush(); //Flush data and write to SD card

  geigerFile.close(); //Close the current data file
  
  //BNO Setup.........................................................

  //ADC (ADS115) Setup.........................................................


  //Serial UV Setup.........................................................


  
  //Pre-Flight Readouts.................................................................
  #ifdef DEBUG
    Serial.println(filename1);
    Serial.println(dataheader);
  #endif
  /* Add in pre-flight readouts such as cut-off altitude etc.*/

  

}


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

   
    
  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity

  //State 1 Timer Reset.....................................................
  if (millis() - state1timer >= data_time ) { 
    state1timer = millis(); // reset the timer
    

    
    
    //Geiger Data Collection.....................................................


    
    //BNO Data Collection........................................................
    


    //LED Data Collection........................................................

 

    //Data collection for print..................................................
    // String header: "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";
;
    
    //Assign collected data to current data array
    float currentdata[24]={millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis(), millis(),millis(),millis(),millis(),millis(),millis(),millis(),millis()};


    
    for (int X=0; X<23; X++){
      
      #ifdef DEBUG
        Serial.print(currentdata[X]);
      #endif
      
      dataFile.print(currentdata[X]);
          if (X<23) {
            
            #ifdef DEBUG
              Serial.print(",");
            #endif
            
            dataFile.print(",");
          }
    };

    #ifdef DEBUG
      Serial.println();
    #endif
    
    dataFile.println();
    
    //Flushing data and reseting counter......................................
    dataFile.flush(); //Flush data and write to SD card


    /*
    //This loop rights from current data into the data array.
    
    for (int j =0; j < 20; j++) {
      data[row_count][j]=currentdata[0][j];
    }
    row_count++; //Increase counter by one to write to the next row of the 2D data array
    */
  }

  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================
  /*
  // State 2: switch LoRa modes (CG)................................................
  if ( (GPStime() - state2time) > 2min ) {
    switch(LoRaMode) {
      case 1: {
        //switch to case 2
        LoRaMode = 2;
        }
        break;
      case 2: {
        //switch to case 3
        }
        LoRaMode = 3;
        break;
      case 3: {
        //switch to case 1
        LoRaMode = 1;
        }   
        break;
     }
  }
  */

  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)


  //State 3 timer reset.....................................................
  if ( (millis() - state3timer) >= SD_time ) {
    state3timer = millis(); //Reset the timer.

    //Geiger Data Collection.....................................................

    for (int i = 0; i < 3; i++) {
      gCounts[i] = gCounter[i];     // Log separate Geiger counter events in 3elem array
      gCounter[i] = 0;              // Reset Geiger interrupt counters
    }

    
  }

  
  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION

  // State 4 timer..........................................................




  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here
  


  
}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

void ISR_GC1() {
  
  gCounter[0] = gCounter[0] + 1;
  geigerFile = SD.open(filename2, FILE_WRITE);
  String G1 = String(millis()) + String(", gCounter 1, ") + String(gCounter[0]);
  geigerFile.println(G1);
  geigerFile.close();

}

void ISR_GC2() {
  gCounter[1] = gCounter[1] + 1;
  geigerFile = SD.open(filename2, FILE_WRITE);
  String G1 = String(millis()) + String(", gCounter 2, ") + String(gCounter[1]);
  geigerFile.println(G1);
  geigerFile.close();
}

void ISR_GC3() {
  gCounter[2] = gCounter[2] + 1;
  geigerFile = SD.open(filename2, FILE_WRITE);
  String G1 = String(millis()) + String(", gCounter 3, ") + String(gCounter[2]);
  geigerFile.println(G1);
  geigerFile.close();
}


//=================================================================================


