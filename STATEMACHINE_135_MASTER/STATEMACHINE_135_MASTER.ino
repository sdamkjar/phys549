/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/


//========================================================================
// ~~~~~ Libraries to include ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>
#include <RH_RF95.h>


//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

// Frequency of data collection to Feather RAM & writing to SD
#define data_time 100           
// Altitude limit for cutdown relay
#define cutoffAlt 30000       
// How long to leave the relay active
#define burn_time 5000
// How long we will wait to until we retry the burn
#define retry_time 5000

// Debug variables ......................................................
// Turn off debug mode for data collection by commenting out this line below
//#define DEBUG_DATA 1  

// Turn off debug mode for geiger collection by commenting out this line below
//#define DEBUG_GEIGER 1

// set timers prior to running the loop...................................
uint32_t data_timer = millis();     // On-board clock timer for data collection state

// GPS Variable definition................................................

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy
uint8_t Lon=0;
uint8_t Lat=0;
uint8_t Alt=0;

// SD Card ..............................................................

const int chipSelect = 4; // Selecting chip for the SD Card
File dataFile;   // Name of the datafile stored on the SD card for logging data in state 1.
File geigerFile; // Name of the datafile stored on the SD card for logging and time stamping geiger data.
char filename1[] = "LOGGER00.txt";
char filename2[] = "GEIGER00.txt";

//Adafruit ID assigning..................................................

Adafruit_BNO055 bno = Adafruit_BNO055(55); // Assign a unique ID to the BNO055
Adafruit_ADS1115 ads1115; //Assign a unique ID to the ADC
Adafruit_VEML6070 uv = Adafruit_VEML6070();   //Assign a unique ID to the VEML6070

// Pin selection stuff...................................................

#define shutdownPin 14
#define relayPin    18
#define GeigerPin1  15
#define GeigerPin2  16
#define GeigerPin3  17

// Data Collection Variables.............................................
float tempCelsius1, tempCelsius2;             // Deal with this later on temp/UV integration
float GUVA_B_volt;
float UV_A_dig;               
uint32_t gCounter1, gCounter2, gCounter3;
uint32_t gCounts1, gCounts2, gCounts3; 
uint32_t gDataArray[100][2];                   // Column1 = pin#, column2 = time in millis
uint16_t gArrayCtr = 0;                       // Will keep track of entries in the array

//Relay Variables........................................................
int relayBurned = 0; //Relay burn counter
int delayRelay = 0;
int relayBurn_try=0; //How many times did the burn cut off occur

// LoRa Variable Definition........................................................


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  delay(1000);
  Serial.begin(115200);   //Set USB serial speed
  while (!Serial) delay(1);
  #ifdef DEBUG_DATA
    Serial.println("HAB Balloon Data Test"); Serial.print("");
  #endif
  
  //LoRa Mode Setup......................................................

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

  pinMode(GeigerPin1, INPUT);
  pinMode(GeigerPin2, INPUT);
  pinMode(GeigerPin3, INPUT);
  attachInterrupt(digitalPinToInterrupt(GeigerPin1), ISR_GC1, RISING); 
  attachInterrupt(digitalPinToInterrupt(GeigerPin2), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GeigerPin3), ISR_GC3, FALLING);
  
  //Cutdown Relay Setup...................................................
  
  pinMode(relayPin,OUTPUT);
  
  //Shutdown Setup........................................................
  
  pinMode(shutdownPin,INPUT); 
  digitalWrite(shutdownPin,HIGH);

  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status, comment out to keep quiet


  delay(1000); //Think it allows 1 update from the GPS first.
  
  //SD CARD SETUP.........................................................

  #ifdef DEBUG_DATA
    Serial.print("Initializing SD card...");
    // make sure that the default chip select pin is set to
    // output, even if you don't use it:
  #endif
  
  pinMode(SS, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    #ifdef DEBUG_DATA
    Serial.println("Card failed, or not present");
    // don't do anything more:
    #endif
    while (1) ;
  }

  #ifdef DEBUG_DATA
  Serial.println("card initialized.");
  #endif
    // create a new file
    

  for (uint8_t i = 0; i < 100; i++) {
    filename1[6] = i/10 + '0';
    filename1[7] = i%10 + '0';
    if (! SD.exists(filename1)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename1, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String dataheader = "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";
  dataFile.println(dataheader);
  
  dataFile.flush(); //Flush data and write to SD card

  dataFile.close(); //Close the current data file

  for (uint8_t i = 0; i < 100; i++) {
    filename2[6] = i/10 + '0';
    filename2[7] = i%10 + '0';
    if (! SD.exists(filename2)) {
      // only open a new file if it doesn't exist
      geigerFile = SD.open(filename2, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String geigerheader = "Clock_Time, Geiger Counter";
  
  geigerFile.println(geigerheader);
  
  geigerFile.flush(); //Flush data and write to SD card

  geigerFile.close(); //Close the current data file
  
  
  //BNO Setup.........................................................
  if(!bno.begin())  //Initialise the BNO sensor
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    #ifdef DEBUG_DATA
      Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    #endif
    while(1);
  }    
  bno.setExtCrystalUse(true);

  //ADC (ADS115) Setup.........................................................
  ads1115.begin();
  ads1115.setGain(GAIN_TWOTHIRDS); // Set ADC input gain to 2/3 so that range is from 0V to 6.144V
  
  //Serial UV Setup.........................................................
  uv.begin(VEML6070_HALF_T);  // pass in the integration time constant

  
  //Pre-Flight Readouts.................................................................
  #ifdef DEBUG_DATA
    Serial.println(filename1);
    Serial.println(dataheader);
  #endif
  
  /* Add in pre-flight readouts such as cut-off altitude etc.*/

  

}

// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

    //GPS Data Collection.....................................................
  
    // Maybe purge this whole thing PURGE IT ALL
    
    // in case you are not using the interrupt above, you'll
    // need to 'hand query' the GPS, not suggested :(
    if (! usingInterrupt) {
      // read data from the GPS in the 'main loop'
      char c = GPS.read();
      // if you want to debug, this is a good time to do it!
  
    }
  
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }
    
  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Timer Reset.....................................................
  if (millis() - data_timer >= data_time ) { 
    data_timer = millis(); // reset the timer
  

    //Loop to continue logging GPS data regardless of if there is a fix or not.
    if (GPS.fix) 
    {
      Lon=GPS.lon;
      Lat=GPS.lat;
      Alt=GPS.altitude;
    }
    else
    {
      Lon=0;
      Lat=0;
      Alt=0;
    }
    //Geiger Data Collection.....................................................
    
    gCounts1 = gCounter1;
    gCounts2 = gCounter2;
    gCounts3 = gCounter3;
    gCounter1 = 0;
    gCounter2 = 0;
    gCounter3 = 0;
    
//    Serial.print("Geiger counts:   ");
//    Serial.print(gCounts1); Serial.print("   ");
//    Serial.print(gCounts2); Serial.print("   ");
//    Serial.print(gCounts3); Serial.print("   ");
//    Serial.println();


    //BNO Data Collection........................................................
    
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER); 
    //Getting Euler variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    
    imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); 
    //Getting Rotation Rate variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    
    int8_t bno_temp = bno.getTemp();
    
    //Temp & UV Data Collection........................................................
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    float tempVolt1 = ads1115.readADC_SingleEnded(0)*6.144/32768;         // digital representation of the voltage from one of the temperature sensors
    tempCelsius1 = (tempVolt1 - 0.5)/0.01;
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    float tempVolt2 = ads1115.readADC_SingleEnded(1)*6.144/32768;         // digital representation of the voltage from one of the temperature sensors
    tempCelsius2 = (tempVolt2 - 0.5)/0.01;
    delay(10);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    GUVA_B_volt = ads1115.readADC_SingleEnded(2)*6.144/32768;          // digital representation of the voltage from the GUVA (analog UV sensor)
    
    UV_A_dig = uv.readUV();                             // digital representation of the voltage from the UVA
    
    Serial.print(GUVA_B_volt); Serial.print(",");
    Serial.println(UV_A_dig);

    //Data collection for print..................................................
    // String header: "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";

    //Assign collected data to current data array
    float currentdata[24]={millis(),GPS.hour,GPS.minute,GPS.seconds,GPS.milliseconds,GPS.fix,GPS.lat,GPS.lon,GPS.altitude,euler.x(),euler.y(),euler.z(),gyro.x(),gyro.y(),gyro.z(), GUVA_B_volt, UV_A_dig,gCounts1,gCounts2,gCounts3,relayBurn_try,bno_temp,tempCelsius1,tempCelsius2};
    
    //Write to SD Card if the shutdown pin isn't low........................................................
    if (digitalRead(shutdownPin) == HIGH) {
      dataFile = SD.open(filename1, FILE_WRITE);
      for (int X=0; X<=23; X++){
        #ifdef DEBUG_DATA
        Serial.print(currentdata[X]);
        #endif
        dataFile.print(currentdata[X]);
          if (X<23) {
            #ifdef DEBUG_DATA
            Serial.print(",");
            #endif
            dataFile.print(",");
          }
      }
      #ifdef DEBUG_DATA
      Serial.println();
      #endif
      dataFile.println();
      //Flushing data and reseting counter......................................
      dataFile.flush(); //Flush data and write to SD card
      dataFile.close(); //Close the data file after
    }

  }

  // ~~~~~ BEGIN GEIGER SD WRITING STATE ~~~~~
  //========================================================================

  if ( gArrayCtr > 40 && digitalRead(shutdownPin) == HIGH ) { 
    geigerFile = SD.open(filename2, FILE_WRITE);
    
    //Write raw Geiger data to SD card
    for (int i = 0; i <= gArrayCtr; i++) { 
      #ifdef DEBUG_GEIGER
      Serial.print(gDataArray[i][1]);
      Serial.print(",");
      Serial.print(gDataArray[i][2]);
      Serial.println();
      #endif
      geigerFile.print(gDataArray[i][1]);
      geigerFile.print(",");
      geigerFile.print(gDataArray[i][2]);
      geigerFile.println();
    }
    //Flushing data
    geigerFile.flush(); //Flush data and write to SD card
    geigerFile.close(); //Close the data file after
    
    // Clear out data array and reset counter
    gDataArray[100][2] = {0};
    gArrayCtr = 0;
    
  }



  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here

  
  // Cutoff relay altitude check...........................................
  if ( Alt > cutoffAlt && relayBurned == 0) {
    // set pin high to activate relay
    digitalWrite(relayPin, HIGH); 
    delayRelay = millis();
    relayBurned = 1;
    relayBurn_try++;
    
  }
  // Relay shutoff...........................................
  // Relay switch to low voltage - turns the COM to NC
  
  else if ((millis() - delayRelay)>= retry_time+burn_time && relayBurned==1){
    relayBurned = 0;
  } 
  
  else if (millis() - delayRelay >= burn_time) {
    digitalWrite(relayPin, LOW); 
  }
  
}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

void ISR_GC1() {
  gCounter1 = gCounter1 + 1;
  gDataArray[gArrayCtr][1] = 1;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter
}

void ISR_GC2() {
  gCounter2 = gCounter2 + 1;
  gDataArray[gArrayCtr][1] = 2;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter  
}

void ISR_GC3() {
  gCounter3 = gCounter3 + 1;
  gDataArray[gArrayCtr][1] = 3;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter    
}


//=================================================================================

