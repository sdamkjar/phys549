/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Gardiner
 * Created: July 26, 2017
 * 
 ***************************************************************************************************/

//========================================================================
// ~~~~~ Libraries required ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>
#include <RH_RF95.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~

#define DEBUG = true //Turn on and off debug mode by setting DEBUG as "true" or "false"


// GPS Variable definition................................................

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy
uint8_t Lon=0;
uint8_t Lat=0;
uint8_t Alt=0;


// LoRa Variables.........................................................

// Pulse width on reset assert (msec)
#define RH_RF95_RESET_DELAY 10 
// Must match frequency of target (MHz)
#define RF95_FREQ           433.0 
// Packet length (RH_RF95_MAX_MESSAGE_LEN = 255 - RH_RF95_HEADER_LEN)
#define RADIO_PACKET_LEN    20 
// Transmit attempt timeout (msec)
#define TX_TIMEOUT          1000 
// Time between sending packets (msec)
#define TX_PERIOD           5000 

// Pin assignments for LoRa SPI interface
#define RFM95_CS            10
#define RFM95_INT           6
#define RFM95_RST           9
#define LED                 8


// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

uint8_t lora_mode = RH_RF95_LORA_MODE_1;

int lora_init(uint8_t newMode);


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  #ifdef DEBUG      
    Serial.begin(115200);      //Set USB serial speed
    Serial.print("HAB Balloon Data Test"); Serial.print("");
    while (!Serial) delay(1);
  #endif
  
  //LoRa RX Mode Setup......................................................
  pinMode(LED, OUTPUT);     
  pinMode(RFM95_RST, OUTPUT);

  Serial.begin(115200);
  while (!Serial) {
    /*Do nothing*/
  }  

  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status, comment out to keep quiet


  delay(1000); // Allows 1 update from the GPS first.

}

// LoRa TX code outside of setup and loop..................................................
int16_t packetnum = 0;  // packet counter, we increment per xmission

// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

  //GPS Data Collection.....................................................
      
  // in case you are not using the interrupt above, you'll
  // need to 'hand query' the GPS, not suggested :(
  if (! usingInterrupt) {
    // read data from the GPS in the 'main loop'
    char c = GPS.read();
    // if you want to debug, this is a good time to do it!
  }

  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived()) {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences! 
    // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
    //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
  
    if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }
  

  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================

  //State 2 Description.....................................................

  if (GPS.minute % 10 >= 0 && GPS.minute % 10 < 3)
  {
    lora_mode = RH_RF95_LORA_MODE_1; // set LoRa mode 1 when minutes is 0<=t<3
  }
  else if (GPS.minute % 10 >= 3 && GPS.minute % 10 < 6)
  {
    lora_mode = RH_RF95_LORA_MODE_2; // set LoRa mode 2 when minutes is 3<=t<6
  }
  else if (GPS.minute % 10 >= 6 && GPS.minute % 10 <= 9)
  {
    lora_mode = RH_RF95_LORA_MODE_3; // set LoRa mode 3 when minutes is 6<=t<9
  }

    // static int n = 0;
    static int statetimer = 0;

    if (millis()-statetimer > 10000)
    {
      statetimer = millis();
      Serial.print("\n\rmode = ");
      Serial.println(lora_mode);
      Serial.print("minute = ");
      Serial.print(GPS.minute);
      Serial.print(",");
      Serial.print(GPS.minute%10);
    }


  //========================================================================


  if( lora_init(lora_mode) == RH_RF95_ERR_NO_CHANGE)
  {
    if (rf95.available())
    {
      // Should be a message for us now   
      uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
      uint8_t len = sizeof(buf);
      
      if (rf95.recv(buf, &len))
      {
        digitalWrite(LED, HIGH);

        RH_RF95::printBuffer("\n\rReceived: ", buf, len);
        Serial.print("Got: ");
        Serial.print("\tFREQ\tPKT#\tTIME\t\tFIX\tLAT\tLONG\tALT\tGUVA\tUVA\tGC0\tGC1\tGC2\tBURN\n\r\t");
        Serial.println((char*)buf);
        Serial.print("RSSI: ");
        Serial.println(rf95.lastRssi(), DEC);
        Serial.print("\n\rmode = ");
        Serial.println(lora_mode);
        Serial.print("minute = ");
        Serial.print(GPS.minute);
        Serial.print(",");
        Serial.print(GPS.minute%10);
        
        // Send a reply
        // uint8_t data[] = "And hello back to you";
        // rf95.send(data, sizeof(data));
        // rf95.waitPacketSent();
        // Serial.println("Sent a reply");

        digitalWrite(LED, LOW);
      }
      else
      {
        Serial.println("Receive failed");
      }
    }
  }



}


// ~~~~~ LoRa Mode Switching Subfunction ~~~~~
//================================================================================

  // LoRa Mode Switching Subfunction description...................................................
    //Fill in description here

int lora_init(uint8_t newMode){

    static uint8_t          mode      = RH_RF95_LORA_MODE_INITIAL;
    static unsigned long    resetWait = 0;
    static bool             resetting = false;

    if (mode == newMode)
    {        
        return RH_RF95_ERR_NO_CHANGE;
    }

    if (!resetting)
    {
        digitalWrite(RFM95_RST, LOW);
        resetWait = millis();
    }

    resetting = true;

    if (millis() - resetWait < RH_RF95_RESET_DELAY)
    {
        return RH_RF95_ERR_RESET_WAIT;
    }

    digitalWrite(RFM95_RST, HIGH);

    resetting = false;

    if (!rf95.init())
    {
    #ifdef DEBUG
        Serial.print("\n\rERROR! LoRa radio init failed.");
    #endif
        return RH_RF95_ERR_LORA_INIT;
    }

    if (!rf95.setFrequency(RF95_FREQ)) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! setFrequency failed.");
    #endif
        return RH_RF95_ERR_SET_FREQ;
    }

    rf95.setTxPower(20, false);

    rf95.sleep();

    if (rf95.setOPModeLoRa() != RH_RF95_ERR_NO_ERROR) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! Failed to enter LoRa Mode.");
    #endif
        return RH_RF95_ERR_LORA_MODE;
    }

    rf95.setModeIdle();

    switch(newMode){
    case RH_RF95_LORA_MODE_DEFAULT:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    case RH_RF95_LORA_MODE_1:
      rf95.setBandWidth(RH_RF95_BW_500KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_8);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_4096CPS);
    break;
    case RH_RF95_LORA_MODE_2:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_7);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_1024CPS);
    break;
    case RH_RF95_LORA_MODE_3:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_6);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    }

    rf95.readRegModemConfig();

    mode = newMode;

    return RH_RF95_ERR_NO_ERROR;

}

