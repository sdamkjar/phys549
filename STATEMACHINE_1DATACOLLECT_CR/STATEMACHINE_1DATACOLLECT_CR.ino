/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/


//========================================================================
// ~~~~~ Libraries to include ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

uint32_t data_time=100; // sample rate to read data to timer
uint32_t LoRa_mode_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000; // Time between SD write
uint32_t LoRa_push_time=5000; // Time between LoRa push

//#define DEBUG = true //Turn on and off debug mode by setting DEBUG as "true" or "false"

// set timers prior to running the loop...................................
uint32_t state1timer = millis(); //on-board clock
uint32_t state2timer = millis(); //GPS time
uint32_t state3timer = millis(); //on-board clock
uint32_t state4timer = millis(); //on-board clock

// GPS Variable definition................................................

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy
uint8_t Lon=0;
uint8_t Lat=0;
uint8_t Alt=0;

// SD Card ..............................................................

const int chipSelect = 4; // Selecting chip for the SD Card
File dataFile;   // Name of the datafile stored on the SD card for logging data in state 1.
File geigerFile; // Name of the datafile stored on the SD card for logging and time stamping geiger data.
char filename1[] = "LOGGER00.txt";
char filename2[] = "GEIGER00.txt";

//Adafruit ID assigning..................................................

Adafruit_BNO055 bno = Adafruit_BNO055(55); // Assign a unique ID to the BNO055
Adafruit_ADS1115 ads1115; //Assign a unique ID to the ADC
Adafruit_VEML6070 uv = Adafruit_VEML6070();   //Assign a unique ID to the VEML6070

// Pin selection stuff...................................................


// Data Collection Variables.............................................
int16_t temp1, temp2; //Assigning photometry sensor variables.
float GUVA, UV_A; //Assigning photometry sensor variables.
uint32_t gCounter[3];             // ISR = Geiger counter logs cosmic ray events--> gCounter[3] = AND output 
uint32_t gCounts[3];              // --> used to log data at state1rate increments

//Relay Variables........................................................
int relayBurn_try=0; //How many times did the burn cut off occur

// LoRa TX Variable Definition........................................................

// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{

  #ifdef DEBUG
    Serial.begin(115200);   //Set USB serial speed
    Serial.print("HAB Balloon Data Test"); Serial.print("");
    while (!Serial) delay(1);
  #endif
  //LoRa Mode Setup......................................................

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

  
  //Cutdown Relay Setup...................................................
  

  
  //Shutdown Setup........................................................
  


  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status, comment out to keep quiet


  delay(1000); //Think it allows 1 update from the GPS first.
  
  //SD CARD SETUP.........................................................
  #ifdef DEBUG
    Serial.print("Initializing SD card...");
    // make sure that the default chip select pin is set to
    // output, even if you don't use it:
  #endif
  
  pinMode(SS, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    #ifdef DEBUG
    Serial.println("Card failed, or not present");
    // don't do anything more:
    #endif
    while (1) ;
  }

  #ifdef DEBUG
  Serial.println("card initialized.");
  #endif
    // create a new file
    

  for (uint8_t i = 0; i < 100; i++) {
    filename1[6] = i/10 + '0';
    filename1[7] = i%10 + '0';
    if (! SD.exists(filename1)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename1, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String dataheader = "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try, bno_temp, temp1, temp2";
  dataFile.println(dataheader);
  
  dataFile.flush(); //Flush data and write to SD card

  dataFile.close(); //Close the current data file

  for (uint8_t i = 0; i < 100; i++) {
    filename2[6] = i/10 + '0';
    filename2[7] = i%10 + '0';
    if (! SD.exists(filename2)) {
      // only open a new file if it doesn't exist
      geigerFile = SD.open(filename2, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  String geigerheader = "Clock_Time, Geiger Counter";
  
  geigerFile.println(geigerheader);
  
  geigerFile.flush(); //Flush data and write to SD card

  geigerFile.close(); //Close the current data file
  

  
  //BNO Setup.........................................................
  if(!bno.begin())  //Initialise the BNO sensor
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    #ifdef DEBUG
      Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    #endif
    while(1);
  }    
  
  bno.setExtCrystalUse(true);

  //ADC (ADS115) Setup.........................................................
  ads1115.begin();

  //Serial UV Setup.........................................................
  uv.begin(VEML6070_1_T);  // pass in the integration time constant

  
  //Pre-Flight Readouts.................................................................
  #ifdef DEBUG
    Serial.println(filename1);
    Serial.println(dataheader);
  #endif

  /* Add in pre-flight readouts such as cut-off altitude etc.*/
  

}


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

    //GPS Data Collection.....................................................
  
    // Maybe purge this whole thing PURGE IT ALL
    
    // in case you are not using the interrupt above, you'll
    // need to 'hand query' the GPS, not suggested :(
    if (! usingInterrupt) {
      // read data from the GPS in the 'main loop'
      char c = GPS.read();
      // if you want to debug, this is a good time to do it!

  
    }
  
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }
    
  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity

  //State 1 Timer Reset.....................................................
  if (millis() - state1timer >= data_time ) { 
    state1timer = millis(); // reset the timer
  
    
    
    //Loop to continue logging GPS data regardless of if there is a fix or not.
    if (GPS.fix) 
    {
      Lon=GPS.lon;
      Lat=GPS.lat;
      Alt=GPS.altitude;
    }
    else
    {
      Lon=0;
      Lat=0;
      Alt=0;
    }
    

    //BNO Data Collection........................................................
    
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER); 
    //Getting Euler variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    
    imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); 
    //Getting Rotation Rate variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()

    int8_t bno_temp = bno.getTemp();
    
    //LED Data Collection........................................................
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    temp1 = ads1115.readADC_SingleEnded(0);         // digital representation of the voltage from one of the temperature sensors
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    temp2 = ads1115.readADC_SingleEnded(1);         // digital representation of the voltage from one of the temperature sensors
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    GUVA = ads1115.readADC_SingleEnded(2);          // digital representation of the voltage from the GUVA (analog UV sensor)
    UV_A = uv.readUV();                             // digital representation of the voltage from the UVA
 

    //Data collection for print..................................................
    // String header: "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z, bno_temp, temp1, temp2,  GUVA, UV_A, gCounts0, gCounts1, gCount2, relayBurn_try";

    //Assign collected data to current data array
    float currentdata[24]={millis(),GPS.hour,GPS.minute,GPS.seconds,GPS.milliseconds,GPS.fix,GPS.lat,GPS.lon,GPS.altitude,euler.x(),euler.y(),euler.z(),gyro.x(),gyro.y(),gyro.z(),bno_temp,temp1,temp2, GUVA, UV_A,gCounts[0],gCounts[1],gCounts[2],relayBurn_try};

    Serial.print(GUVA); Serial.print(",");
    Serial.println(UV_A);
    //Write to SD Card (Serial print in this code)...........................................................
    //
   dataFile = SD.open(filename1, FILE_WRITE);
    
    for (int X=0; X<23; X++){
      
      #ifdef DEBUG
        Serial.print(currentdata[X]);
      #endif
      
      dataFile.print(currentdata[X]);
          if (X<22) {
            
            #ifdef DEBUG
              Serial.print(",");
            #endif
            
            dataFile.print(",");
          }
    };

    #ifdef DEBUG
      Serial.println();
    #endif
    
    dataFile.println();
    
    //Flushing data and reseting counter......................................
    dataFile.flush(); //Flush data and write to SD card
    
    dataFile.close(); //Close the data file afterj

    
  }

    
  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================
  /*
  // State 2: switch LoRa modes (CG)................................................
  if ( (GPStime() - state2time) > 2min ) {
    switch(LoRaMode) {
      case 1: {
        //switch to case 2
        LoRaMode = 2;
        }
        break;
      case 2: {
        //switch to case 3
        }
        LoRaMode = 3;
        break;
      case 3: {
        //switch to case 1
        LoRaMode = 1;
        }   
        break;
     }
  }
  */

  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)

    //Geiger Data Collection.....................................................

  if ( (millis() - state3timer) >= SD_time ) {
    state3timer = millis(); //Reset the timer.
    
    //Geiger Data Collection.....................................................

    for (int i = 0; i < 3; i++) {
      gCounts[i] = 1;     // Log separate Geiger counter events in 3elem array
      gCounter[i] = 0;              // Reset Geiger interrupt counters
    }

    
  }




  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION



  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here
  



}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================




//=================================================================================

