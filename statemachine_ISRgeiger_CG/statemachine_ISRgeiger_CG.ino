/***************************************************************************************************
 * Description: Has set-up for interrupt pins from Geiger counters. Also has ISRs coded up (at the
 * bottom) and a quick data collection method in for loop. 
 * 
 * 
 * Author: C. Gardiner
 * Created: July 26, 2017
 * 
 ***************************************************************************************************/

//========================================================================
// ~~~~~ Libraries required ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

#define data_time 100

// set timers prior to running the loop...................................

uint32_t data_timer = millis();

// GPS Variable definition................................................


// SD Card ..............................................................

// Adafruit ID assigning..................................................

// Pin selection stuff...................................................

#define GeigerPin1 15       
#define GeigerPin2 16 
#define GeigerPin3 17

// Data Collection Variables.............................................
uint32_t gCounter1, gCounter2, gCounter3;
uint32_t gCounts1, gCounts2, gCounts3; 
uint16_t gDataArray[100][2];                   // Column1 = pin#, column2 = time in millis
uint16_t gArrayCtr = 0;                       // Will keep track of entries in the array

// Relay Variables........................................................


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  //LoRa Mode Setup......................................................


  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................
  pinMode(GeigerPin1, INPUT);
  pinMode(GeigerPin2, INPUT);
  pinMode(GeigerPin3, INPUT);
  attachInterrupt(digitalPinToInterrupt(GeigerPin1), ISR_GC1, RISING); 
  attachInterrupt(digitalPinToInterrupt(GeigerPin2), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GeigerPin3), ISR_GC3, FALLING); 

 
  //Cutdown Relay Setup...................................................
  
  
  //Shutdown Setup........................................................
  

  //GPS Setup.............................................................

  
  //SD CARD SETUP.........................................................

  
  //BNO Setup.........................................................


  //ADC (ADS115) Setup.........................................................


  //Serial UV Setup.........................................................

  
  //Pre-Flight Readouts.................................................................

}


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

  // ~~~~~ BEGIN DATA COLLECTION & SD WRITING STATE ~~~~~
  //========================================================================

  if ( millis() - data_timer > data_time ) { 

    data_timer = millis(); // reset the timer

    gCounts1 = gCounter1;
    gCounts2 = gCounter2;
    gCounts3 = gCounter3;
    gCounter1 = 0;
    gCounter2 = 0;
    gCounter3 = 0;

    Serial.print("Geiger counts:   ");
    Serial.print(gCounts1); Serial.print("   ");
    Serial.print(gCounts2); Serial.print("   ");
    Serial.print(gCounts3); Serial.print("   ");
    Serial.println();
  }



  // ~~~~~ BEGIN GEIGER SD WRITING STATE ~~~~~
  //========================================================================

  if ( gArrayCtr > 75 ) { 

    //Write raw Geiger data to SD card
//    for (int i = 0; i <= gArrayCtr; i++) { 
//      Serial.print(gDataArray[i][1]);
//      Serial.print(",");
//      Serial.print(gDataArray[i][2]);
//      Serial.println();
//    }

    // Clear out data array and reset counter
    gDataArray[100][2] = {0};
    gArrayCtr = 0;
    
  }


}



// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================


void ISR_GC1() {
  gCounter1 = gCounter1 + 1;
  gDataArray[gArrayCtr][1] = 1;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter
}

void ISR_GC2() {
  gCounter2 = gCounter2 + 1;
  gDataArray[gArrayCtr][1] = 2;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter  
}

void ISR_GC3() {
  gCounter3 = gCounter3 + 1;
  gDataArray[gArrayCtr][1] = 3;         // Pin number
  gDataArray[gArrayCtr][2] = millis();  // Log time of event
  gArrayCtr = gArrayCtr + 1;            // increase array counter    
}

//=================================================================================

