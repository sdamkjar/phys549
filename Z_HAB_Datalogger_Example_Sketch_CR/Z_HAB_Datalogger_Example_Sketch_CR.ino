// ~~~~~ HAB Datalogger Sketch Details~~~~~
// Author: C. Robson, Date Created: 2017-07-17, Date Last Modified: 2017-07-24

// ~~~~~ Sketch Description ~~~~~

/* Put shit in here to describe the program */

/* List of improvements
 *  
 *  1.
 *  2.
 *  3.
 */

// Libraries to include

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>

// ~~~~~ Board Specific Definition ~~~~

// GPS Variable definition

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
#define GPSECHO false /* Set to 'false' to turn off gps echo to serial, and to true if it should echo to serial */
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy

// SD Card 
const int chipSelect = 4; // Selecting chip for the SD Card
File dataFile; // Name of the datafile stored on the SD card 

//Adafruit BNO stuff


Adafruit_BNO055 bno = Adafruit_BNO055(55); /* Assign a unique ID to this sensor at the same time */

void setup() 
{

  Serial.print("Orientation Sensor Test"); Serial.print("");

  // Unsure what this is
  //#ifndef ESP8266
    //while (!Serial); // for Leonardo/Micro/Zero
  //#endif

  //Set serial speed
  Serial.begin(115200);

  // ~~~~~ GPS SETUP ~~~~~~

  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);

  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  
  // uncomment this line to turn on only the "minimum recommended" data
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
  // the parser doesn't care about other sentences at this time
  
  // Set the update rate
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  // For the parsing code to work nicely and have time to sort thru the data, and
  // print it out we don't suggest using anything higher than 1 Hz

  // Request updates on antenna status, comment out to keep quiet
  GPS.sendCommand(PGCMD_ANTENNA);

  #ifdef __arm__
  usingInterrupt = false;  //NOTE - we don't want to use interrupts on the Due
  #else
  useInterrupt(true);
  #endif

  delay(1000);
  
  // Ask for firmware version
  mySerial.print(PMTK_Q_RELEASE);  
  
  // ~~~~~ End GPS Setup ~~~~~


  // ~~~~~ SD CARD SETUP ~~~~~

  Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(SS, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1) ;
  }
  Serial.println("card initialized.");
  

    // create a new file
  char filename[] = "LOGGER00.txt";
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i/10 + '0';
    filename[7] = i%10 + '0';
    if (! SD.exists(filename)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename, FILE_WRITE); 
      break;  // leave the loop!
    }
  }
  
  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  
  delay(10000);
    
  bno.setExtCrystalUse(true);
  Serial.println(filename);


}


#ifdef __AVR__
// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
#ifdef UDR0
  if (GPSECHO)
    if (c) UDR0 = c;  
    // writing direct to UDR0 is much much faster than Serial.print 
    // but only one character can be written at a time. 
#endif
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }
  String Header="TIME, LAT, LONG, FIX, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z";
  dataFile.println(header);
  Serial.println(header);
}

#endif //#ifdef__AVR__
uint32_t data_timer=100; // sample rate to read data to timer
uint32_t timer = millis(); //The timer that is used throughout the code



void loop() {
  
  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())  timer = millis();
  
{
  // in case you are not using the interrupt above, you'll
  // need to 'hand query' the GPS, not suggested :(
  if (! usingInterrupt) {
    // read data from the GPS in the 'main loop'
    char c = GPS.read();
    // if you want to debug, this is a good time to do it!
    if (GPSECHO)
      if (c) Serial.print(c);

  }

  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived()) {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences! 
    // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
    //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
  
    if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }



  // approximately every 1-2 seconds or so, print out the current stats
  if (millis() - timer > data_timer ) { 
    timer = millis(); // reset the timer

    //Get sensor event from the BNO
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
    //imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
    int8_t euler_x=euler.x(); 
    int8_t euler_y=euler.y();
    int8_t euler_z=euler.z();

    
    Serial.print("\nTime: ");
    Serial.print(GPS.hour, DEC); Serial.print(':');
    Serial.print(GPS.minute, DEC); Serial.print(':');
    Serial.print(GPS.seconds, DEC); Serial.print('.');
    Serial.println(GPS.milliseconds);
    Serial.print("Date: ");
    Serial.print(GPS.day, DEC); Serial.print('/');
    Serial.print(GPS.month, DEC); Serial.print("/20");
    Serial.println(GPS.year, DEC);
    Serial.print("Fix: "); Serial.print((int)GPS.fix);
    Serial.print(" quality: "); Serial.println((int)GPS.fixquality); 
    if (GPS.fix) {
      Serial.print("Location: ");
      Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
      Serial.print(", "); 
      Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);
      Serial.print("Speed (knots): "); Serial.println(GPS.speed);
      Serial.print("Angle: "); Serial.println(GPS.angle);
      Serial.print("Altitude: "); Serial.println(GPS.altitude);
      Serial.print("Satellites: "); Serial.println((int)GPS.satellites);
      Serial.print("X: ");
      Serial.print(event.orientation.x, 4);
      Serial.print("\tY: ");
      Serial.print(event.orientation.y, 4);
      Serial.print("\tZ: ");
      Serial.print(event.orientation.z, 4);
      Serial.print("\tTemp:  ");
      Serial.print(temp);
      Serial.println("");
      /* Get a new sensor event*/
      unsigned long Millis1 = millis(); // grab current time
    
      // make a string for assembling the data to log:
      String dataString = "";
      
      dataString +="X: "; dataString +=event.orientation.x,4; dataString +=",  ";
      dataString +="Y: "; dataString +=event.orientation.y,4; dataString +=",  ";
      dataString +="Z: "; dataString +=event.orientation.z,4; dataString +=",  "; 
      dataString +="Temp:  "; dataString +=temp; dataString +=millis();
    
      //dataFile.println(dataString);
    
    

      // print to the serial port too:
      //Serial.println(dataString);
    
      
      // The following line will 'save' the file to the SD card after every
      // line of data - this will use more power and slow down how much data
      // you can read but it's safer! 
      // If you want to speed up the system, remove the call to flush() and it
      // will save the file only every 512 bytes - every time a sector on the 
      // SD card is filled with data.
      
      //dataFile.flush();
      unsigned long Millis2 = millis(); // grab current time
    }
  }
  
  /* Get a new sensor event from the BNO*/



  
  


  
 
 

  
  }
}
