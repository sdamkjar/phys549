# LoRa Interface

## Initializing LoRa

LoRa must be reset and initialized to change the transmit configuration. This is to prevent possible issues caused by attempting to change settings while sending a packet, which would out the LoRa in an inoperable state.

Changing the variable `lora_state` will prompt the LoRa to reset and initialize. The pin marked `LORA_RST` (pin 9) in the schematic will be held low for 10 msec and then set high before configuring the registers. The reset pin number and reset period are defined in `RH_RF95.h` as `RHM95_RST` and `RH_RF95_RESET_DELAY` respectively and can be changed if necessary.

To avoid disturbing other functions, the reset delay is achieved with a reference to `millis()`. The function `lora_init` should be called on each iteration of the state machine loop. The function will return an error code indicating if no mode change is needed, if a reset is underway, or if an error has occured.

## Error Codes

Error codes for the LoRa interface defined in `RH_RF95.h` are listed below.

```C++
// ERROR CODES
#define RH_RF95_ERR_NO_ERROR						  0x00
#define RH_RF95_ERR_BAD_READ						  0x01
#define RH_RF95_ERR_BAD_WRITE						  0x02
#define RH_RF95_ERR_LORA_NO_SLEEP           	      0x03
#define RH_RF95_ERR_NO_CHANGE						  0x04
#define RH_RF95_ERR_RESET_WAIT						  0x05
#define RH_RF95_ERR_LORA_INIT						  0x06
#define RH_RF95_ERR_SET_FREQ						  0x07
#define RH_RF95_ERR_LORA_MODE						  0x08
#define RH_RF95_ERR_INVALID_MODE					  0x09
```

## Changing Modes

Changing the global variable `lora_mode` will trigger a reset and initialization of the LoRa board the next time `int lora_init(uint8_t newMode)` is called. The value of `lora_mode` must be one of the valid values listed below, otherwise an error code `RH_RF95_ERR_INVALID_MODE` will be returned.

```C++
// MISSION MODES
#define RH_RF95_LORA_MODE_DEFAULT 					  0x00
#define RH_RF95_LORA_MODE_1       					  0x01
#define RH_RF95_LORA_MODE_2       					  0x02
#define RH_RF95_LORA_MODE_3       					  0x03
```

Varying the bit rate using the LoRa modulation requires that three parameters be configured. The Bandwidth, the Coding Rate, and the Spreading Factor of these radios can be configured to a range of values. Analysis of the different combinations is required to define the impact on communication range, signal strength, and whether the communication will be signal or noise limited.
A calculator for verifying the resulting levels of the signal for each configuration is available here http://www.semtech.com/apps/filedown/down.php?file=SX1272LoRaCalculatorSetup1%271.zip
The different modes have been selected to test the capabilities of the LoRa radio. They differ in bandwidth, coding rate, and spreading factor. These values are listed below for each mode.

|Mode                      |Bandwidth |Coding Rate |Spreading Factor |Equivalent Data Rate |
|--------------------------|----------|------------|-----------------|---------------------|
|RH_RF95_LORA_MODE_DEFAULT |125 kHz   |4/5         |128              |5468.75 bps          |
|RH_RF95_LORA_MODE_1       |250 kHz   |4/5         |4096             |585.94 bps           |
|RH_RF95_LORA_MODE_2       |250 kHz   |4/7         |1024             |1395.09 bps          |
|RH_RF95_LORA_MODE_3       |125 kHz   |4/6         |128              |4557.29 bps          |

## Hardware Interfacing

The LoRa board is connected to the Feather Arduino with the following pins. Pins that have a define can be reconfigured if necessary.

|Pin Function    |Arduino Pin |LoRa Pin    |Define    |
|----------------|------------|------------|----------|
|Power Supply    |3V3         |VIN         |          |
|Ground          |GND         |GND         |          |
|Enable          |            |EN          |          |
|Interrupt       |D6          |G0          |RFM95_INT |
|SPI Clock       |D13         |SCK         |          |
|SPI MISO        |D12         |MISO        |          |
|SPI MOSI        |D11         |MOSI        |          |
|SPI Chip Select |D10         |CS          |RFM95_CS  |
|Reset           |D9          |RST         |RFM95_RST |

## SPI Driver

The ATSAMD21 microcontroller on the Feather Arduino has only one dedicated SPI peripheral. This is used by the SD card. A second SPI interface is created using the SerCom interface. 

```C++
SPIClass mySPI (&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);
```

`mySPI` is initialized within `RHSPIDriver.cpp` and can only be called within that file. Public functions for using this SPI interface are:

```C++
bool    RHSPIDriver::init()
uint8_t RHSPIDriver::spiRead(uint8_t reg)
uint8_t RHSPIDriver::spiWrite(uint8_t reg, uint8_t val)
uint8_t RHSPIDriver::spiBurstRead(uint8_t reg, uint8_t* dest, uint8_t len)
uint8_t RHSPIDriver::spiBurstWrite(uint8_t reg, const uint8_t* src, uint8_t len)
void    RHSPIDriver::setSlaveSelectPin(uint8_t slaveSelectPin)
uint8_t RHSPIDriver::spiTransfer(uint8_t val)
```

## RadioHead Library

The radio library must be modified to add compatibility for the SerCom interface, error codes, and mode switching. The following files in the RadioHead library should be replaced with the modified versions.

* `RH_RF95.cpp`
* `RH_RF95.h`
* `RHSPIDriver.cpp`

The modified versions are available here:
<https://bitbucket.org/sdamkjar/phys549/src/de86f324ea01bc5055f65f4c8706fc8d8ae2b944/RadioHead%20Mods/?at=master> 

## Fault Handling

Incase the LoRa board enters a bad mode, attempts to interface with it should return an error code. Incase an interaction with the LoRa board returns a bad error code 5 times, the the Arduino will attempt to reset and initialize the LoRa board the next time `int lora_init(uint8_t newMode)`. The number of retry attempts is defined by `RH_RF95_RETRIES` in `RH_RF95.h` and can be changed in necessary.

