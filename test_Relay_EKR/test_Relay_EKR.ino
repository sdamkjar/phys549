// Pin used by the Relay, A4 = 18
int relayPin = 18;
// Set the GPS at ground altitude 0
int GPS_altitude = 0;
/* Names used in the others code
 * GPS.altitude //Altitude reported by the GPS 
 * altLimit    // Altitude limit that we set before the mission starts
*/
void setup(){
  // Open Serial port. Sets data rate to 9600 bps (bits per second (baud))
  Serial.begin(9600);
  // Set the pin for outputing.
  pinMode(relayPin, OUTPUT);
  // Sets the LED off
  digitalWrite(relayPin, LOW); 
}

void loop(){

  // The time only starts at zero only once at the first loop
  static int startTime   = millis();
  static int delayRelay  = 0;
  static int relayBurned = 0;
  
  // Start 10 seconds after arduino starts
  if (millis () - startTime >= 10000) {
    
  // GPS gives the max altitude of 30 km 
    GPS_altitude = 30000;   
  }
  
  // If GPS is at the given altitude, turn the relay on, else, turn it off.
    if(GPS_altitude >= 30000 && relayBurned == 0 ) { 
      
  // Relay switch to High Voltage - turns the COM to NO
      digitalWrite(relayPin, HIGH); 
      delayRelay = millis();
      relayBurned = 1;
    }
    
  // Relay switch to low voltage - turns the COM to NC
    else if (millis() - delayRelay > 15000) {
      digitalWrite(relayPin, LOW); 
    }
}
