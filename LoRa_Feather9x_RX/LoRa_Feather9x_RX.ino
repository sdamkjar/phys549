// Feather9x_RX
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (receiver)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Feather9x_TX

#include <SPI.h>
#include <RH_RF95.h>

#define DEBUG 1



// Pulse width on reset assert (msec)
#define RH_RF95_RESET_DELAY 10
// Must match frequency of target (MHz)
#define RF95_FREQ           915.0
// Packet length (RH_RF95_MAX_MESSAGE_LEN = 255 - RH_RF95_HEADER_LEN)
#define RADIO_PACKET_LEN    20
// Transmit attempt timeout (msec)
#define TX_TIMEOUT          1000
// Time between sending packets (msec)
#define TX_PERIOD           5000
// Pin assignments for LoRa SPI interface
#define RFM95_CS            10
#define RFM95_INT           6
#define RFM95_RST           9
#define LED 8

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

uint8_t lora_mode = RH_RF95_LORA_MODE_3;

int lora_init(uint8_t newMode);

// Blinky on receipt
#define LED 8

void setup() 
{
  pinMode(LED, OUTPUT);     
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  while (!Serial);
  Serial.begin(115200);
  delay(100);

  
  
}

void loop()
{
  if( lora_init(lora_mode) == RH_RF95_ERR_NO_CHANGE)
  {
    if (rf95.available())
    {
      // Should be a message for us now   
      uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
      uint8_t len = sizeof(buf);
      
      if (rf95.recv(buf, &len))
      {
        digitalWrite(LED, HIGH);
        RH_RF95::printBuffer("Received: ", buf, len);
        Serial.print("Got: ");
        Serial.println((char*)buf);
         Serial.print("RSSI: ");
        Serial.println(rf95.lastRssi(), DEC);
        
        // Send a reply
        uint8_t data[] = "And hello back to you";
        rf95.send(data, sizeof(data));
        rf95.waitPacketSent();
        Serial.println("Sent a reply");
        digitalWrite(LED, LOW);
      }
      else
      {
        Serial.println("Receive failed");
      }
    }
  }
}



int lora_init(uint8_t newMode){

    static uint8_t          mode      = RH_RF95_LORA_MODE_INITIAL;
    static unsigned long    resetWait = 0;
    static bool             resetting = false;

    if (mode == newMode)
    {
        return RH_RF95_ERR_NO_CHANGE;
    }

    if (!resetting)
    {
        digitalWrite(RFM95_RST, LOW);
        resetWait = millis();
    }

    resetting = true;

    if (millis() - resetWait < RH_RF95_RESET_DELAY)
    {
        return RH_RF95_ERR_RESET_WAIT;
    }

    digitalWrite(RFM95_RST, HIGH);

    resetting = false;

    if (!rf95.init())
    {
    #ifdef DEBUG
        Serial.print("\n\rERROR! LoRa radio init failed.");
    #endif
        return RH_RF95_ERR_LORA_INIT;
    }

    if (!rf95.setFrequency(RF95_FREQ)) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! setFrequency failed.");
    #endif
        return RH_RF95_ERR_SET_FREQ;
    }

    rf95.setTxPower(20, false);

    rf95.sleep();

    if (rf95.setOPModeLoRa() != RH_RF95_ERR_NO_ERROR) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! Failed to enter LoRa Mode.");
    #endif
        return RH_RF95_ERR_LORA_MODE;
    }

    rf95.setModeIdle();

    switch(newMode){
    case RH_RF95_LORA_MODE_DEFAULT:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    case RH_RF95_LORA_MODE_1:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_4096CPS);
    break;
    case RH_RF95_LORA_MODE_2:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_7);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_1024CPS);
    break;
    case RH_RF95_LORA_MODE_3:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_6);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    }

    rf95.readRegModemConfig();

    mode = newMode;

    return RH_RF95_ERR_NO_ERROR;

}
