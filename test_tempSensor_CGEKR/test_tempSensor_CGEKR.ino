/* Code to read temperatures and UV readings from the ADC. 
 *  
 *  TMP36 website: https://www.adafruit.com/product/165
 *  
 *  ADC datasheet: in the Google Drive
 *  
 */
 
#include <Wire.h>
#include <Adafruit_ADS1015.h>   
//#include "Adafruit_VEML6070.h"

Adafruit_ADS1115 ads1115; 
//Adafruit_VEML6070 uv = Adafruit_VEML6070();   //Digital UV

void setup(void)
{
  Serial.begin(9600);
  ads1115.setGain(GAIN_TWOTHIRDS); // Set ADC input gain to 2/3 so that range is from 0V to 6.144V
  ads1115.begin();
//  uv.begin(VEML6070_1_T);  // pass in the integration time constant
  
}

void loop(void)
{
  int16_t tempDig1;
  float tempVoltage, tempCelsius;

  tempDig1 = ads1115.readADC_SingleEnded(0);    // Read the digital representation of temp sensor 1
  tempVoltage = tempDig1 * 6.144;               // Multiply digital signal by max range of input voltage
                                                // -> 6.144V comes from Gain = 2/3
  tempVoltage /= 32768;                         // 32768 is the number value of 15 bits (2^15)
  tempCelsius = (tempVoltage - 0.5)/0.01;       // Temp conversion from TMP36 
    // ^^ 0degC = 0.5V (offset), and 10mV/C after the offset
  delay(10);                                    // Delay inbetween pin read - for sample rate of ADC?

  Serial.print("Raw bits: "); Serial.println(tempDig1);
  Serial.print("Voltage: "); Serial.print(tempVoltage); Serial.println(" V");
  Serial.print("Temperature: "); Serial.print(tempCelsius); Serial.println(" C");
  Serial.println();

  delay(1000);   // amount of time between samples
   
  //-----------------------------------------------------------------------------------------------------------------

} 
