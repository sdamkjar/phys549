// RH_RF95.cpp
//
// Copyright (C) 2011 Mike McCauley
// $Id: RH_RF95.cpp,v 1.14 2017/03/04 00:59:41 mikem Exp $

#include <RH_RF95.h>

// Interrupt vectors for the 3 Arduino interrupt pins
// Each interrupt can be handled by a different instance of RH_RF95, allowing you to have
// 2 or more LORAs per Arduino
RH_RF95* RH_RF95::_deviceForInterrupt[RH_RF95_NUM_INTERRUPTS] = {0, 0, 0};
uint8_t RH_RF95::_interruptCount = 0; // Index into _deviceForInterrupt for next device

// These are indexed by the values of ModemConfigChoice
// Stored in flash (program) memory to save SRAM
PROGMEM static const RH_RF95::ModemConfig MODEM_CONFIG_TABLE[] =
{
    //  1d,     1e,      26
    { 0x72,   0x74,    0x00}, // Bw125Cr45Sf128 (the chip default)
    { 0x92,   0x74,    0x00}, // Bw500Cr45Sf128
    { 0x48,   0x94,    0x00}, // Bw31_25Cr48Sf512
    { 0x78,   0xc4,    0x00}, // Bw125Cr48Sf4096
    
};

RH_RF95::RH_RF95(uint8_t slaveSelectPin, uint8_t interruptPin, RHGenericSPI& spi)
    :
    RHSPIDriver(slaveSelectPin, spi),
    _rxBufValid(0)
{
    _interruptPin = interruptPin;
    _myInterruptIndex = 0xff; // Not allocated yet
}

bool RH_RF95::init()
{
    if (!RHSPIDriver::init())
	return false;

    // Determine the interrupt number that corresponds to the interruptPin
    int interruptNumber = digitalPinToInterrupt(_interruptPin);
    if (interruptNumber == NOT_AN_INTERRUPT)
	return false;
#ifdef RH_ATTACHINTERRUPT_TAKES_PIN_NUMBER
    interruptNumber = _interruptPin;
#endif

    // No way to check the device type :-(
    
    // Set sleep mode, so we can also set LORA mode:
    spiWrite(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE);
    delay(10); // Wait for sleep mode to take over from say, CAD
    // Check we are in sleep mode, with LORA set
    if (spiRead(RH_RF95_REG_01_OP_MODE) != (RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE))
    {
	Serial.println(spiRead(RH_RF95_REG_01_OP_MODE), HEX);
	return false; // No device present?
    }

    // Add by Adrien van den Bossche <vandenbo@univ-tlse2.fr> for Teensy
    // ARM M4 requires the below. else pin interrupt doesn't work properly.
    // On all other platforms, its innocuous, belt and braces
    pinMode(_interruptPin, INPUT); 

    // Set up interrupt handler
    // Since there are a limited number of interrupt glue functions isr*() available,
    // we can only support a limited number of devices simultaneously
    // ON some devices, notably most Arduinos, the interrupt pin passed in is actuallt the 
    // interrupt number. You have to figure out the interruptnumber-to-interruptpin mapping
    // yourself based on knwledge of what Arduino board you are running on.
    if (_myInterruptIndex == 0xff)
    {
	// First run, no interrupt allocated yet
	if (_interruptCount <= RH_RF95_NUM_INTERRUPTS)
	    _myInterruptIndex = _interruptCount++;
	else
	    return false; // Too many devices, not enough interrupt vectors
    }
    _deviceForInterrupt[_myInterruptIndex] = this;
    if (_myInterruptIndex == 0)
	attachInterrupt(interruptNumber, isr0, RISING);
    else if (_myInterruptIndex == 1)
	attachInterrupt(interruptNumber, isr1, RISING);
    else if (_myInterruptIndex == 2)
	attachInterrupt(interruptNumber, isr2, RISING);
    else
	return false; // Too many devices, not enough interrupt vectors

    // Set up FIFO
    // We configure so that we can use the entire 256 byte FIFO for either receive
    // or transmit, but not both at the same time
    spiWrite(RH_RF95_REG_0E_FIFO_TX_BASE_ADDR, 0);
    spiWrite(RH_RF95_REG_0F_FIFO_RX_BASE_ADDR, 0);

    // Packet format is preamble + explicit-header + payload + crc
    // Explicit Header Mode
    // payload is TO + FROM + ID + FLAGS + message data
    // RX mode is implmented with RXCONTINUOUS
    // max message data length is 255 - 4 = 251 octets

    setModeIdle();

    // Set up default configuration
    // No Sync Words in LORA mode.
    setModemConfig(Bw125Cr45Sf128); // Radio default
    // setModemConfig(Bw125Cr48Sf4096); // slow and reliable?
    setPreambleLength(8); // Default is 8
    // An innocuous ISM frequency, same as RF22's
    setFrequency(434.0);
    // Lowish power
    setTxPower(13);

    return true;
}

// C++ level interrupt handler for this instance
// LORA is unusual in that it has several interrupt lines, and not a single, combined one.
// On MiniWirelessLoRa, only one of the several interrupt lines (DI0) from the RFM95 is usefuly 
// connnected to the processor.
// We use this to get RxDone and TxDone interrupts
void RH_RF95::handleInterrupt()
{
    // Read the interrupt register
    uint8_t irq_flags = spiRead(RH_RF95_REG_12_IRQ_FLAGS);
    if (_mode == RHModeRx && irq_flags & (RH_RF95_RX_TIMEOUT | RH_RF95_PAYLOAD_CRC_ERROR))
    {
	_rxBad++;
    }
    else if (_mode == RHModeRx && irq_flags & RH_RF95_RX_DONE)
    {
	// Have received a packet
	uint8_t len = spiRead(RH_RF95_REG_13_RX_NB_BYTES);

	// Reset the fifo read ptr to the beginning of the packet
	spiWrite(RH_RF95_REG_0D_FIFO_ADDR_PTR, spiRead(RH_RF95_REG_10_FIFO_RX_CURRENT_ADDR));
	spiBurstRead(RH_RF95_REG_00_FIFO, _buf, len);
	_bufLen = len;
	spiWrite(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags

	// Remember the last signal to noise ratio, LORA mode
	// Per page 111, SX1276/77/78/79 datasheet
	_lastSNR = (int8_t)spiRead(RH_RF95_REG_19_PKT_SNR_VALUE) / 4;

	// Remember the RSSI of this packet, LORA mode
	// this is according to the doc, but is it really correct?
	// weakest receiveable signals are reported RSSI at about -66
	_lastRssi = spiRead(RH_RF95_REG_1A_PKT_RSSI_VALUE);
	// Adjust the RSSI, datasheet page 87
	if (_lastSNR < 0)
	    _lastRssi = _lastRssi + _lastSNR;
	else
	    _lastRssi = (int)_lastRssi * 16 / 15;
	if (_usingHFport)
	    _lastRssi -= 157;
	else
	    _lastRssi -= 164;
	    
	// We have received a message.
	validateRxBuf(); 
	if (_rxBufValid)
	    setModeIdle(); // Got one 
    }
    else if (_mode == RHModeTx && irq_flags & RH_RF95_TX_DONE)
    {
	_txGood++;
	setModeIdle();
    }
    else if (_mode == RHModeCad && irq_flags & RH_RF95_CAD_DONE)
    {
        _cad = irq_flags & RH_RF95_CAD_DETECTED;
        setModeIdle();
    }
    
    spiWrite(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
}

// These are low level functions that call the interrupt handler for the correct
// instance of RH_RF95.
// 3 interrupts allows us to have 3 different devices
void RH_RF95::isr0()
{
    if (_deviceForInterrupt[0])
	_deviceForInterrupt[0]->handleInterrupt();
}

void RH_RF95::isr1()
{
    if (_deviceForInterrupt[1])
	_deviceForInterrupt[1]->handleInterrupt();
}

void RH_RF95::isr2()
{
    if (_deviceForInterrupt[2])
	_deviceForInterrupt[2]->handleInterrupt();
}

// Check whether the latest received message is complete and uncorrupted
void RH_RF95::validateRxBuf()
{
    if (_bufLen < 4)
	return; // Too short to be a real message
    // Extract the 4 headers
    _rxHeaderTo    = _buf[0];
    _rxHeaderFrom  = _buf[1];
    _rxHeaderId    = _buf[2];
    _rxHeaderFlags = _buf[3];
    if (_promiscuous ||
	_rxHeaderTo == _thisAddress ||
	_rxHeaderTo == RH_BROADCAST_ADDRESS)
    {
	_rxGood++;
	_rxBufValid = true;
    }
}

bool RH_RF95::available()
{
    if (_mode == RHModeTx)
	return false;
    setModeRx();
    return _rxBufValid; // Will be set by the interrupt handler when a good message is received
}

void RH_RF95::clearRxBuf()
{
    ATOMIC_BLOCK_START;
    _rxBufValid = false;
    _bufLen = 0;
    ATOMIC_BLOCK_END;
}

bool RH_RF95::recv(uint8_t* buf, uint8_t* len)
{
    if (!available())
	return false;
    if (buf && len)
    {
	ATOMIC_BLOCK_START;
	// Skip the 4 headers that are at the beginning of the rxBuf
	if (*len > _bufLen-RH_RF95_HEADER_LEN)
	    *len = _bufLen-RH_RF95_HEADER_LEN;
	memcpy(buf, _buf+RH_RF95_HEADER_LEN, *len);
	ATOMIC_BLOCK_END;
    }
    clearRxBuf(); // This message accepted and cleared
    return true;
}

bool RH_RF95::send(const uint8_t* data, uint8_t len)
{
    if (len > RH_RF95_MAX_MESSAGE_LEN)
	return false;

    waitPacketSent(); // Make sure we dont interrupt an outgoing message
    setModeIdle();

    if (!waitCAD()) 
	return false;  // Check channel activity

    // Position at the beginning of the FIFO
    spiWrite(RH_RF95_REG_0D_FIFO_ADDR_PTR, 0);
    // The headers
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderTo);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderFrom);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderId);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderFlags);
    // The message data
    spiBurstWrite(RH_RF95_REG_00_FIFO, data, len);
    spiWrite(RH_RF95_REG_22_PAYLOAD_LENGTH, len + RH_RF95_HEADER_LEN);

    setModeTx(); // Start the transmitter
    // when Tx is done, interruptHandler will fire and radio mode will return to STANDBY
    return true;
}

bool RH_RF95::send(const uint8_t* data, uint8_t len, uint16_t timeout)
{
    if (len > RH_RF95_MAX_MESSAGE_LEN)
    return false;

    waitPacketSent(timeout); // Make sure we dont interrupt an outgoing message
    if (!setModeIdle())
    return false;

    if (!waitCAD()) 
    return false;  // Check channel activity

    // Position at the beginning of the FIFO
    spiWrite(RH_RF95_REG_0D_FIFO_ADDR_PTR, 0);
    // The headers
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderTo);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderFrom);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderId);
    spiWrite(RH_RF95_REG_00_FIFO, _txHeaderFlags);
    // The message data
    spiBurstWrite(RH_RF95_REG_00_FIFO, data, len);
    spiWrite(RH_RF95_REG_22_PAYLOAD_LENGTH, len + RH_RF95_HEADER_LEN);

    if (!setModeTx()) // Start the transmitter
    return false;
    // when Tx is done, interruptHandler will fire and radio mode will return to STANDBY
    return true;
}

bool RH_RF95::printRegisters()
{
#ifdef RH_HAVE_SERIAL
    uint8_t registers[] = { 0x01, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x014, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27};

    uint8_t i;
    for (i = 0; i < sizeof(registers); i++)
    {
	Serial.print(registers[i], HEX);
	Serial.print(": ");
	Serial.println(spiRead(registers[i]), HEX);
    }
#endif
    return true;
}

bool RH_RF95::printRegister(uint8_t reg)
{  
#ifdef RH_HAVE_SERIAL
    Serial.print(reg, HEX);
    Serial.print(" ");
    Serial.println(spiRead(reg), HEX);
#endif
    return true;
}

uint8_t RH_RF95::maxMessageLength()
{
    return RH_RF95_MAX_MESSAGE_LEN;
}

bool RH_RF95::setFrequency(float centre)
{
    // Frf = FRF / FSTEP
    uint32_t frf = (centre * 1000000.0) / RH_RF95_FSTEP;
    spiWrite(RH_RF95_REG_06_FRF_MSB, (frf >> 16) & 0xff);
    spiWrite(RH_RF95_REG_07_FRF_MID, (frf >> 8) & 0xff);
    spiWrite(RH_RF95_REG_08_FRF_LSB, frf & 0xff);
    _usingHFport = (centre >= 779.0);

    return true;
}

bool RH_RF95::setModeIdle()
{
    if (_mode != RHModeIdle)
    {
	    if (setRegister(RH_RF95_REG_01_OP_MODE,RH_RF95_MODE,RH_RF95_MODE_STDBY)
            != RH_RF95_ERR_NO_ERROR)
        {
        #ifdef RH_HAVE_SERIAL
            Serial.print("\n\rERROR! Failed to enter standby mode.");
        #endif
            return false;
        }
	   _mode = RHModeIdle;
    }
    return true;
}

bool RH_RF95::sleep()
{
    if (_mode != RHModeSleep)
    {
        if (setRegister(RH_RF95_REG_01_OP_MODE,RH_RF95_MODE,RH_RF95_MODE_SLEEP)
            != RH_RF95_ERR_NO_ERROR)
        {
        #ifdef RH_HAVE_SERIAL
            Serial.print("\n\rERROR! Failed to enter sleep mode.");
        #endif
            return false;
        } 
	   _mode = RHModeSleep;
    }
    return true;
}

bool RH_RF95::setModeRx()
{
    if (_mode != RHModeRx)
    {
        if (setRegister(RH_RF95_REG_01_OP_MODE,RH_RF95_MODE,RH_RF95_MODE_RXCONTINUOUS)
            != RH_RF95_ERR_NO_ERROR)
        {
        #ifdef RH_HAVE_SERIAL
            Serial.print("\n\rERROR! Failed to enter RX mode.");
        #endif
            return false;
        } 
	    spiWrite(RH_RF95_REG_40_DIO_MAPPING1, 0x00); // Interrupt on RxDone
	    _mode = RHModeRx;
    }
    return true;
}

bool RH_RF95::setModeTx()
{
    if (_mode != RHModeTx)
    {
        if (setRegister(RH_RF95_REG_01_OP_MODE,RH_RF95_MODE,RH_RF95_MODE_TX)
            != RH_RF95_ERR_NO_ERROR)
        {
        #ifdef RH_HAVE_SERIAL
            Serial.print("\n\rERROR! Failed to enter TX mode.");
        #endif
            return false;
        } 
    	spiWrite(RH_RF95_REG_40_DIO_MAPPING1, 0x40); // Interrupt on TxDone
    	_mode = RHModeTx;
    }
    return true;
}

void RH_RF95::setTxPower(int8_t power, bool useRFO)
{
    // Sigh, different behaviours depending on whther the module use PA_BOOST or the RFO pin
    // for the transmitter output
    if (useRFO)
    {
	if (power > 14)
	    power = 14;
	if (power < -1)
	    power = -1;
	spiWrite(RH_RF95_REG_09_PA_CONFIG, RH_RF95_MAX_POWER | (power + 1));
    }
    else
    {
	if (power > 23)
	    power = 23;
	if (power < 5)
	    power = 5;

	// For RH_RF95_PA_DAC_ENABLE, manual says '+20dBm on PA_BOOST when OutputPower=0xf'
	// RH_RF95_PA_DAC_ENABLE actually adds about 3dBm to all power levels. We will us it
	// for 21, 22 and 23dBm
	if (power > 20)
	{
	    spiWrite(RH_RF95_REG_4D_PA_DAC, RH_RF95_PA_DAC_ENABLE);
	    power -= 3;
	}
	else
	{
	    spiWrite(RH_RF95_REG_4D_PA_DAC, RH_RF95_PA_DAC_DISABLE);
	}

	// RFM95/96/97/98 does not have RFO pins connected to anything. Only PA_BOOST
	// pin is connected, so must use PA_BOOST
	// Pout = 2 + OutputPower.
	// The documentation is pretty confusing on this topic: PaSelect says the max power is 20dBm,
	// but OutputPower claims it would be 17dBm.
	// My measurements show 20dBm is correct
	spiWrite(RH_RF95_REG_09_PA_CONFIG, RH_RF95_PA_SELECT | (power-5));
    }
}

// Sets registers from a canned modem configuration structure
void RH_RF95::setModemRegisters(const ModemConfig* config)
{
    spiWrite(RH_RF95_REG_1D_MODEM_CONFIG1,       config->reg_1d);
    spiWrite(RH_RF95_REG_1E_MODEM_CONFIG2,       config->reg_1e);
    spiWrite(RH_RF95_REG_26_MODEM_CONFIG3,       config->reg_26);
}

// Set one of the canned FSK Modem configs
// Returns true if its a valid choice
bool RH_RF95::setModemConfig(ModemConfigChoice index)
{
    if (index > (signed int)(sizeof(MODEM_CONFIG_TABLE) / sizeof(ModemConfig)))
        return false;

    ModemConfig cfg;
    memcpy_P(&cfg, &MODEM_CONFIG_TABLE[index], sizeof(RH_RF95::ModemConfig));
    setModemRegisters(&cfg);

    return true;
}

void RH_RF95::setPreambleLength(uint16_t bytes)
{
    spiWrite(RH_RF95_REG_20_PREAMBLE_MSB, bytes >> 8);
    spiWrite(RH_RF95_REG_21_PREAMBLE_LSB, bytes & 0xff);
}

bool RH_RF95::isChannelActive()
{
    // Set mode RHModeCad
    if (_mode != RHModeCad)
    {
        spiWrite(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_CAD);
        spiWrite(RH_RF95_REG_40_DIO_MAPPING1, 0x80); // Interrupt on CadDone
        _mode = RHModeCad;
    }

    while (_mode == RHModeCad)
        YIELD;

    return _cad;
}

void RH_RF95::enableTCXO()
{
    while ((spiRead(RH_RF95_REG_4B_TCXO) & RH_RF95_TCXO_TCXO_INPUT_ON) != RH_RF95_TCXO_TCXO_INPUT_ON)
    {
	sleep();
	spiWrite(RH_RF95_REG_4B_TCXO, (spiRead(RH_RF95_REG_4B_TCXO) | RH_RF95_TCXO_TCXO_INPUT_ON));
    } 
}

// From section 4.1.5 of SX1276/77/78/79
// Ferror = FreqError * 2**24 * BW / Fxtal / 500
int RH_RF95::frequencyError()
{
    int32_t freqerror = 0;

    // Convert 2.5 bytes (5 nibbles, 20 bits) to 32 bit signed int
    freqerror = spiRead(RH_RF95_REG_28_FEI_MSB) << 16;
    freqerror |= spiRead(RH_RF95_REG_29_FEI_MID) << 8;
    freqerror |= spiRead(RH_RF95_REG_2A_FEI_LSB);
    // Sign extension into top 3 nibbles
    if (freqerror & 0x80000)
	freqerror |= 0xfff00000;

    int error = 0; // In hertz
    float bw_tab[] = {7.8, 10.4, 15.6, 20.8, 31.25, 41.7, 62.5, 125, 250, 500};
    uint8_t bwindex = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1) >> 4;
    if (bwindex < (sizeof(bw_tab) / sizeof(float)))
	error = (float)freqerror * bw_tab[bwindex] * ((float)(1L << 24) / (float)RH_RF95_FXOSC / 500.0);
    // else not defined

    return error;
}

int RH_RF95::lastSNR()
{
    return _lastSNR;
}

int RH_RF95::setOPModeLoRa()
{

    int8_t value = 0;
    value = spiRead(RH_RF95_REG_01_OP_MODE);

    if (value & RH_RF95_MODE != RH_RF95_MODE_SLEEP)
    {
    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rError! Must be in sleep mode to enter LoRa Mode.");
    #endif
        return RH_RF95_ERR_LORA_NO_SLEEP;
    }
    
    spiWrite(RH_RF95_REG_01_OP_MODE,value | RH_RF95_LONG_RANGE_MODE);

    value = spiRead(RH_RF95_REG_01_OP_MODE);

    if (value & RH_RF95_LONG_RANGE_MODE != RH_RF95_LONG_RANGE_MODE){
    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rError! Bad Write in rf95.setOPModeLoRa().");
    #endif
        return RH_RF95_ERR_BAD_WRITE;
    }

    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rSet to LoRa Mode.");
    #endif
    return RH_RF95_ERR_NO_ERROR;
}

int RH_RF95::readRegModemConfig()
{
    int8_t config1 = 0;
    int8_t config2 = 0;

    config1 = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1);
    config2 = spiRead(RH_RF95_REG_1E_MODEM_CONFIG2);

    #ifdef RH_HAVE_SERIAL

        Serial.print("\n\rBandwidth: ");
        switch(config1 & RH_RF95_BW){
            case RH_RF95_BW_7_8KHZ:
                Serial.print("7.8 kHz");
            break;
            case RH_RF95_BW_10_4KHZ:
                Serial.print("10.4 kHz");
            break;
            case RH_RF95_BW_15_6KHZ:
                Serial.print("15.6 kHz");
            break;
            case RH_RF95_BW_20_8KHZ:
                Serial.print("20.8 kHz");
            break;
            case RH_RF95_BW_31_25KHZ:
                Serial.print("31.25 kHz");
            break;
            case RH_RF95_BW_41_7KHZ:
                Serial.print("41.7 kHz");
            break;
            case RH_RF95_BW_62_5KHZ:
                Serial.print("62.5 kHz");
            break;
            case RH_RF95_BW_125KHZ:
                Serial.print("125 kHz");
            break;
            case RH_RF95_BW_250KHZ:
                Serial.print("250 kHz");
            break;
            case RH_RF95_BW_500KHZ:
                Serial.print("500 kHz");
            break;
            default:
                Serial.print(config1 & RH_RF95_BW,HEX);
                Serial.println("\n\rERROR! Bad read.");
                return RH_RF95_ERR_BAD_READ;
        }
        Serial.print("\n\rCoding Rate: ");
        switch(config1 & RH_RF95_CODING_RATE){
            case RH_RF95_CODING_RATE_4_5:
                Serial.print("4/5");
            break;
            case RH_RF95_CODING_RATE_4_6:
                Serial.print("4/6");
            break;
            case RH_RF95_CODING_RATE_4_7:
                Serial.print("4/7");
            break;
            case RH_RF95_CODING_RATE_4_8:
                Serial.print("4/8");
            break;
            default:
                Serial.print(config1 & RH_RF95_CODING_RATE);
                Serial.println("\n\rERROR! Bad read.");
                return RH_RF95_ERR_BAD_READ;
        }
        Serial.print("\n\rSpreading Factor: ");
        switch(config2 & RH_RF95_SPREADING_FACTOR){
            case RH_RF95_SPREADING_FACTOR_64CPS:
                Serial.print("64 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_128CPS:
                Serial.print("128 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_256CPS:
                Serial.print("256 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_512CPS:
                Serial.print("512 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_1024CPS:
                Serial.print("1024 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_2048CPS:
                Serial.print("2048 chips/symbol");
            break;
            case RH_RF95_SPREADING_FACTOR_4096CPS:
                Serial.print("4096 chips/symbol");
            break;
            default:
                Serial.print(config2 & RH_RF95_SPREADING_FACTOR,HEX);
                Serial.println("\n\rERROR! Bad read.");
                return RH_RF95_ERR_BAD_READ;
        }

    #endif

    return RH_RF95_ERR_NO_ERROR;
}

int RH_RF95::setBandWidth(uint8_t newValue)
{
    
    uint8_t value = 0;
    value = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1);

    newValue |= ~RH_RF95_BW;
    value |= RH_RF95_BW;
    newValue &= value;

    spiWrite(RH_RF95_REG_1D_MODEM_CONFIG1,newValue);
    value = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1);

    if(value&RH_RF95_BW != newValue&RH_RF95_BW){
    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rError! Bad Write in rf95.setBandWidth().");
    #endif
        return RH_RF95_ERR_BAD_WRITE;
    }
    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rSet BandWidth.");
    #endif
    return RH_RF95_ERR_NO_ERROR;
}

int RH_RF95::setCodingRate(uint8_t newValue)
{
    
    uint8_t value = 0;
    value = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1);
    
    newValue |= ~RH_RF95_CODING_RATE;
    value |= RH_RF95_CODING_RATE;
    newValue &= value;

    spiWrite(RH_RF95_REG_1D_MODEM_CONFIG1,newValue);
    value = spiRead(RH_RF95_REG_1D_MODEM_CONFIG1);

    if(value&RH_RF95_CODING_RATE != newValue&RH_RF95_CODING_RATE){
    #ifdef RH_HAVE_SERIAL
        Serial.println("Error! Bad Write in rf95.setCodingRate().");
    #endif
        return RH_RF95_ERR_BAD_WRITE;
    }

    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rSet Coding Rate.");
    #endif
    return RH_RF95_ERR_NO_ERROR;
}

int RH_RF95::setSpreadingFactor(uint8_t newValue)
{
    
    uint8_t value = 0;
    value = spiRead(RH_RF95_REG_1E_MODEM_CONFIG2);
    
    newValue |= ~RH_RF95_SPREADING_FACTOR;
    value |= RH_RF95_SPREADING_FACTOR;
    newValue &= value;

    spiWrite(RH_RF95_REG_1E_MODEM_CONFIG2,newValue);
    value = spiRead(RH_RF95_REG_1E_MODEM_CONFIG2);

    if(value&RH_RF95_SPREADING_FACTOR != newValue&RH_RF95_SPREADING_FACTOR){
    #ifdef RH_HAVE_SERIAL
        Serial.println("Error! Bad Write in rf95.setSpreadingFactor().");
    #endif
        return RH_RF95_ERR_BAD_WRITE;
    }

    // #ifdef RH_HAVE_SERIAL
    //     Serial.print("\n\rSet Spreading Factor.");
    // #endif
    return RH_RF95_ERR_NO_ERROR;
}

int RH_RF95::setRegister(uint8_t adr, uint8_t mask, uint8_t newValue)
{
    
    uint8_t value = 0;
    value = spiRead(adr);
    
    newValue |= ~mask;
    value |= mask;
    newValue &= value;

    spiWrite(adr,newValue);
    
    for (int i=0; i<RH_RF95_MAX_RETRIES; i++){
        value = spiRead(adr);
        if (value == newValue) break;
    }
    
    if(value&mask != newValue&mask){
    #ifdef RH_HAVE_SERIAL
        Serial.print("\n\rERROR! Bad write at 0x");
        Serial.print(adr,HEX);
    #endif
        return RH_RF95_ERR_BAD_WRITE;
    }

    // #ifdef RH_HAVE_SERIAL
    //     Serial.print("\n\rSet register 0x");
    //     Serial.print(adr,HEX);
    //     Serial.print(" to 0x");
    //     Serial.print(newValue,HEX);
    //     Serial.print("\n");
    // #endif
    return RH_RF95_ERR_NO_ERROR;
}

