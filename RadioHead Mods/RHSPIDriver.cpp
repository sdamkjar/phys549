// RHSPIDriver.cpp
//
// Copyright (C) 2014 Mike McCauley
// $Id: RHSPIDriver.cpp,v 1.10 2015/12/16 04:55:33 mikem Exp $

#include <RHSPIDriver.h>
#include "wiring_private.h" // pinPeripheral() function

SPIClass mySPI (&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);

RHSPIDriver::RHSPIDriver(uint8_t slaveSelectPin, RHGenericSPI& spi)
    : 
    _spi(spi),
    _slaveSelectPin(slaveSelectPin)
{
}

bool RHSPIDriver::init()
{
    // start the SPI library with the default speeds etc:
    // On Arduino Due this defaults to SPI1 on the central group of 6 SPI pins
    mySPI.begin();

    // Assign pins 11, 12, 13 to SERCOM functionality
    pinPeripheral(11, PIO_SERCOM);
    pinPeripheral(12, PIO_SERCOM);
    pinPeripheral(13, PIO_SERCOM);

    // Initialise the slave select pin
    // On Maple, this must be _after_ spi.begin
    pinMode(_slaveSelectPin, OUTPUT);
    digitalWrite(_slaveSelectPin, HIGH);

    delay(100);

    
    return true;
}

uint8_t RHSPIDriver::spiRead(uint8_t reg)
{
    uint8_t val;
    ATOMIC_BLOCK_START;
#if defined(SPI_HAS_TRANSACTION)
    mySPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
#endif
    digitalWrite(_slaveSelectPin, LOW);
    mySPI.transfer(reg & ~RH_SPI_WRITE_MASK); // Send the address with the write mask off
    val = mySPI.transfer(0); // The written value is ignored, reg value is read
    digitalWrite(_slaveSelectPin, HIGH);
#if defined(SPI_HAS_TRANSACTION)
    mySPI.endTransaction();
#endif
    ATOMIC_BLOCK_END;
    return val;
}

uint8_t RHSPIDriver::spiWrite(uint8_t reg, uint8_t val)
{
    uint8_t status = 0;
    ATOMIC_BLOCK_START;
#if defined(SPI_HAS_TRANSACTION)
    mySPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
#endif
    digitalWrite(_slaveSelectPin, LOW);
    status = mySPI.transfer(reg | RH_SPI_WRITE_MASK); // Send the address with the write mask on
    mySPI.transfer(val); // New value follows
// #ifdef RH_HAVE_SERIAL
//     Serial.print("\n\rTransmitting: 0x");
//     Serial.print(val,HEX);
//     Serial.print(" to register 0x");
//     Serial.print(reg,HEX);
// #endif
    digitalWrite(_slaveSelectPin, HIGH);
#if defined(SPI_HAS_TRANSACTION)
    mySPI.endTransaction();
#endif
    ATOMIC_BLOCK_END;
    return status;
}

uint8_t RHSPIDriver::spiBurstRead(uint8_t reg, uint8_t* dest, uint8_t len)
{
    uint8_t status = 0;
    ATOMIC_BLOCK_START;
#if defined(SPI_HAS_TRANSACTION)
    mySPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
#endif
    digitalWrite(_slaveSelectPin, LOW);
    status = mySPI.transfer(reg & ~RH_SPI_WRITE_MASK); // Send the start address with the write mask off
    while (len--)
	*dest++ = mySPI.transfer(0);
    digitalWrite(_slaveSelectPin, HIGH);
#if defined(SPI_HAS_TRANSACTION)
    mySPI.endTransaction();
#endif
    ATOMIC_BLOCK_END;
    return status;
}

uint8_t RHSPIDriver::spiBurstWrite(uint8_t reg, const uint8_t* src, uint8_t len)
{
    uint8_t status = 0;
    ATOMIC_BLOCK_START;
#if defined(SPI_HAS_TRANSACTION)
    mySPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
#endif
    digitalWrite(_slaveSelectPin, LOW);
    status = mySPI.transfer(reg | RH_SPI_WRITE_MASK); // Send the start address with the write mask on
    while (len--)
	mySPI.transfer(*src++);
    digitalWrite(_slaveSelectPin, HIGH);
#if defined(SPI_HAS_TRANSACTION)
    mySPI.endTransaction();
#endif
    ATOMIC_BLOCK_END;
    return status;
}

void RHSPIDriver::setSlaveSelectPin(uint8_t slaveSelectPin)
{
    _slaveSelectPin = slaveSelectPin;
}

uint8_t RHSPIDriver::spiTransfer(uint8_t val)
{
    uint8_t response = 0;
    response = mySPI.transfer(val); 
    return response;
}