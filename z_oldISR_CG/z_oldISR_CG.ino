/***************************************************************************************************
 * State machine framework for Feather operation in-flight. All blocks of tested code for each 
 * component will be placed in here as completed. This will result in an incrementally consolidated
 * program for running TroLLPIC operations.
 * 
 * Author: Caelia Gardiner
 * Created: July 24, 11:10
 * 
 ***************************************************************************************************/

// Libraries necessary for proper operation.
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
//#include <Adafruit_Sensor.h>
//#include <Adafruit_BNO055.h>
//#include <utility/imumaths.h>
//#include <Adafruit_GPS.h>


//========================================================================
// Macros and global variables.

// for GPS...............................................................


// for radio/LoRa........................................................


// for loop..............................................................
uint32_t state1timer = millis();  // State 1 = reading data to Feather RAM
uint32_t state1rate = 100;        // --> enter State 1 every 100ms (1sec for testing)

uint32_t state2timer = 0;         // State 2 = LoRa mode switching (GPS min chunks)
// ^^ maybe not necessary

uint32_t state3timer = millis();  // State 3 = writing to SD card 
uint32_t state3rate = 1000;       // --> enter State 3 every 1000ms (1sec) (5sec for testing)

uint32_t state4timer = millis();  // State 4 = push to LoRa
uint32_t state4rate = 5000;       // --> enter State 4 every 5000ms (5sec) (10sec for testing)
uint8_t LoRaMode = 1;             // --> either 1, 2, or 3

uint8_t cutoffAlt = 18;           // State 5 = activate relay to cut-down balloon

uint32_t gCounter[3];             // ISR = Geiger counter logs cosmic ray events
                                  // --> gCounter[3] = AND output 
uint32_t gCounts[3];              // --> used to log data at state1rate increments
const byte GeigerPin[3] = {15, 16, 17};       
                                  // Set interrupt pin locations for Geiger counters
                                  // --> pin3 is AND gate

//========================================================================
// Extra set-up functions (if necessary, should be included in "set-up" if possible). 



//========================================================================
// The big set-up function. 

void setup() {

  // GPS set-up code........................................................

  // radio/LoRa set-up code.................................................

  // photometry set-up code.................................................

  // Geiger counter set-up code.............................................
  pinMode(GeigerPin[0], INPUT);
  pinMode(GeigerPin[1], INPUT);
  pinMode(GeigerPin[2], INPUT);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[0]), ISR_GC1, FALLING); 
  attachInterrupt(digitalPinToInterrupt(GeigerPin[1]), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[2]), ISR_GC3, FALLING); 

  // relay set-up code......................................................  

  // BN0 set-up code........................................................  

  // SD set-up code.........................................................

  // testing set-up code....................................................
  Serial.begin(115200);      // this is the data rate of the GPS board
}

//========================================================================
// Main loop function for in-flight software.
void loop() {

  // State 1: collect data and save to RAM .........................
  if ( (millis() - state1timer) > state1rate ){

    // collect data into vectored variables for the following:
    //    GPS position, velocity, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity, temp

    for (int i = 0; i < 3; i++) {
      gCounts[i] = gCounter[i];     // Log separate Geiger counter events in 3elem array
      gCounter[i] = 0;              // Reset Geiger interrupt counters
    }

    state1timer = millis();  

    // For testing:
    Serial.print("State 1:   ");
    for (int i = 0; i < 3; i++) {
      Serial.print(gCounts[i]);
      Serial.print("  ");
    }
    Serial.print("...Time: ");
    Serial.print(state1timer);
    Serial.println();
    
  }

  // State 2: switch LoRa modes ................................................

  // If GPS.minute is 0,1,2 -> LoRa in mode 1
  // If GPS.minute is 3,4,5 -> LoRa in mode 2
  // If GPS.minute is 6,7,8,9 -> LoRa in mode 3

  // State 3: write to SD card .................................................
  if ( (millis() - state3timer) > state3rate ) {

    // write GPS information for each second
    // write the other information for each 100ms sampling rate
    // check "shut-down pin" for HIGH - if so, close SD file

    state3timer = millis();

    Serial.println("State 3");

  }

  // State 4: push to LoRA (CG)....................................................
  if ( (millis() - state4timer) > state4rate ) {

    // push desired data packet to LoRa

    state4timer = millis();

    Serial.println("State 4");

  }

  // State 5: activate the relay (CG).............................................
 
  // If GPS.alt > cutoffAlt, set relay pin high

}

//================================================================================
// ISRs for Geiger counter events.

void ISR_GC1() {
  gCounter[0] = gCounter[0] + 1;
}

void ISR_GC2() {
  gCounter[1] = gCounter[1] + 1;
}

void ISR_GC3() {
  gCounter[2] = gCounter[2] + 1;
}

//=================================================================================
































