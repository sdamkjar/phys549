/* Code to read temperatures and UV readings from the ADC. 
 *  
 *  TMP36 website: https://www.adafruit.com/product/165
 *  
 *  ADC datasheet: in the Google Drive
 *  
 */
 
#include <Wire.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>

Adafruit_ADS1115 ads1115;                     // ADC (temp1, temp2, GUVA)
Adafruit_VEML6070 uv = Adafruit_VEML6070();   // Digital UV
// GUVA is the analog UV sensor that measures UVB radiation
// "Digital UV" measures UVA radiation

void setup(void)
{
  Serial.begin(9600);
  ads1115.setGain(GAIN_TWOTHIRDS); // Set ADC input gain to 2/3 so that range is from 0V to 6.144V
  ads1115.begin();
  uv.begin(VEML6070_HALF_T);          // pass in the integration time constant (??)

  Serial.println("Temp1   Temp2   GUVA_B   UV_A");
 
}

void loop(void)
{
  float tempDig1, tempDig2, GUVA_B_dig;
  uint16_t UV_A_dig;  
  float tempVolt1, tempVolt2, GUVA_B_volt, UV_A_volt;
  float tempCelsius1, tempCelsius2;

  // First temperature sensor:
  tempDig1 = ads1115.readADC_SingleEnded(0);    // Read the digital representation of temp sensor 1
  tempVolt1 = tempDig1 * 6.144;               // Multiply digital signal by max range of input voltage
                                                // -> 6.144V comes from Gain = 2/3
  tempVolt1 /= 32768;                         // 32768 is the number value of 15 bits (2^15)
  tempCelsius1 = (tempVolt1 - 0.5)/0.01;       // Temp conversion from TMP36 
    // ^^ 0degC = 0.5V (offset), and 10mV/C after the offset
  delay(2);                                    // Delay inbetween pin read - for sample rate of ADC?

  // Second temperature sensor:
  tempDig2 = ads1115.readADC_SingleEnded(1);    // Read the digital representation of temp sensor 2
  tempVolt2 = tempDig2 * 6.144;               // Multiply digital signal by max range of input voltage
                                                // -> 6.144V comes from Gain = 2/3
  tempVolt2 /= 32768;                         // 32768 is the number value of 15 bits (2^15)
  tempCelsius2 = (tempVolt2 - 0.5)/0.01;       // Temp conversion from TMP36 
    // ^^ 0degC = 0.5V (offset), and 10mV/C after the offset
  delay(2);                                    // Delay inbetween pin read - for sample rate of ADC?

  // Analog UV sensor (GUVA_B):
  GUVA_B_dig = ads1115.readADC_SingleEnded(2);    // Read the digital representation of GUVA_B
  GUVA_B_volt = GUVA_B_dig * 6.144;               // Multiply digital signal by max range of input voltage
                                                // -> 6.144V comes from Gain = 2/3
  GUVA_B_volt /= 32768;                         // 32768 is the number value of 15 bits (2^15)
  delay(2);                                    // Delay inbetween pin read - for sample rate of ADC?

  // Digital UV sensor (UV_A):
  UV_A_dig = uv.readUV();                       // Read the digital representation of UV_A
  UV_A_volt = UV_A_dig * 6.144;               // Multiply digital signal by max range of input voltage
                                                // -> 6.144V comes from Gain = 2/3
  UV_A_volt /= 32768;                         // 32768 is the number value of 15 bits (2^15)
  delay(2);                                    // Delay inbetween pin read - for sample rate of ADC?


  Serial.print(tempDig1); Serial.print(" C    ");
  Serial.print(tempDig2); Serial.print(" C    ");
  Serial.print(GUVA_B_dig); Serial.print(" V    ");    
  Serial.print(UV_A_dig); Serial.print(" V    ");    
  Serial.println(" ");

  delay(1000);   // amount of time between samples
   
  //-----------------------------------------------------------------------------------------------------------------

} 
