/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/


//========================================================================
// ~~~~~ Libraries to include ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_GPS.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

uint32_t data_time=100; // sample rate to read data to timer
uint32_t LoRa_mode_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000; // Time between SD write
uint32_t LoRa_push_time=5000; // Time between LoRa push
uint32_t cutoffAlt = 100; //Altitude limit
uint32_t burn_time = 15000; //How long to leave the relay active
uint32_t retry_time = 5000; //How long we will wait to until we retry the burn
int LoRaMode = 1;             // --> either 1, 2, or 3

#define DEBUG = true //Turn on and off debug mode by setting DEBUG as "true" or "false"

// set timers prior to running the loop...................................
uint32_t state1timer = millis(); //on-board clock
uint32_t state2timer = millis(); //GPS time
uint32_t state3timer = millis(); //on-board clock
uint32_t state4timer = millis(); //on-board clock
uint32_t relayTest_time=millis(); //timer to test the relay

// GPS Variable definition................................................

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy
uint8_t Lon=0;
uint8_t Lat=0;
uint8_t Alt=0;

// SD Card ..............................................................


//Adafruit ID assigning..................................................



// Pin selection stuff...................................................


const byte relayPin = 18;


// Data Collection Variables.............................................


//Relay Variables........................................................
int relayBurned = 0; //Relay burn counter
int delayRelay = 0;
int relayBurn_try=0; //How many times did the burn cut off occur

// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  
  #ifdef DEBUG
    Serial.begin(115200);   //Set USB serial speed
    Serial.print("HAB Balloon Data Test"); Serial.print("");
    while (!Serial) delay(1);
  #endif

  //LoRa Mode Setup......................................................

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

  
  //Cutdown Relay Setup...................................................
  
  pinMode(relayPin,OUTPUT);
  
  //Shutdown Setup........................................................
  


  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status, comment out to keep quiet


  delay(1000); //Think it allows 1 update from the GPS first.
  
  //SD CARD SETUP.........................................................


  
  //BNO Setup.........................................................


  //ADC (ADS115) Setup.........................................................


  //Serial UV Setup.........................................................


  
  //Pre-Flight Readouts.................................................................

  /* Add in pre-flight readouts such as cut-off altitude etc.*/

  

}


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

    //GPS Data Collection.....................................................
  
    // Maybe purge this whole thing PURGE IT ALL
    
    // in case you are not using the interrupt above, you'll
    // need to 'hand query' the GPS, not suggested :(
    if (! usingInterrupt) {
      // read data from the GPS in the 'main loop'
      char c = GPS.read();
      // if you want to debug, this is a good time to do it!
  
    }
  
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }

    
  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity

  //State 1 Timer Reset.....................................................

    //State 1 Timer Reset.....................................................
  if (millis() - state1timer >= data_time ) { 
    state1timer = millis(); // reset the timer
  

    //Loop to continue logging GPS data regardless of if there is a fix or not.
    if (GPS.fix) 
    {
      Lon=GPS.lon;
      Lat=GPS.lat;
      Alt=GPS.altitude;
    }
    else
    {
      Lon=0;
      Lat=0;
      Alt=0;
    }
    #ifdef DEBUG
      Serial.println(Alt);
    #endif
  }

  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================

  // State 2: switch LoRa modes (CG)................................................
 

  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)



  
  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION

  // State 4 timer..........................................................



  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here

  
  
  // Cutoff relay altitude check...........................................
  if ( Alt > cutoffAlt && relayBurned == 0) {
    // set pin high to activate relay
    digitalWrite(relayPin, HIGH); 
    delayRelay = millis();
    relayBurned = 1;
    relayBurn_try++;
    #ifdef DEBUG
      Serial.print("Burn try");
      Serial.println(relayBurn_try);
    #endif
    
  }
  // Relay shutoff...........................................
  // Relay switch to low voltage - turns the COM to NC
  
  else if ((millis() - delayRelay)>= retry_time+burn_time && relayBurned==1){
    relayBurned = 0;
  } 
  
  else if (millis() - delayRelay >= burn_time) {
    digitalWrite(relayPin, LOW); 
  }
  
}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================


//=================================================================================

