
#include <Wire.h>
#include <Adafruit_ADS1015.h>   
#include "Adafruit_VEML6070.h"

Adafruit_ADS1115 ads1115; 
Adafruit_VEML6070 uv = Adafruit_VEML6070();   //Digital UV

void setup(void)
{
  Serial.begin(9600);
  Serial.println("Hello!");
  ads1115.setGain(GAIN_TWOTHIRDS);
  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: 188uV = 1 bit, 16 bits = 6.144 V");
  ads1115.begin();
  uv.begin(VEML6070_1_T);  // pass in the integration time constant
  
}

void loop(void)
{
  // Inside this is the stuff that is actually needed
  //=======================================================================================================
  // This is where the analog-digital converter reads the analog photometry inputs
  //-----------------------------------------------------------------------------------------------------------------
  int16_t IR_925, IR_880, GUVA, UV_A;
  float IR_925_voltage, IR_880_voltage,GUVA_voltage ,UVA_voltage;
  //int16_t pin3_unused;

  IR_925 = ads1115.readADC_SingleEnded(0);        // digital representation of 925 nm LED voltage
  IR_925_voltage = (IR_925 * 0.1875)/1000;        // 0.1875 is the conversion for bits to voltage, may need to change 
  delay(10);                                       // delay inbetween pin read
  IR_880 = ads1115.readADC_SingleEnded(1);        // digital representation of 880 nm LED voltage
  IR_880_voltage = (IR_880 * 0.1875)/1000;        // 0.1875 is the conversion for bits to voltage, may need to change 
  delay(10);       
  GUVA = ads1115.readADC_SingleEnded(2);          // digital representation of the current from the GUVA
  GUVA_voltage = (GUVA * 0.1875)/1000;            // 0.1875 is the conversion for bits to voltage, may need to change 
  delay(10);
  UV_A = uv.readUV();
  UVA_voltage = (UV_A * 0.1875)/1000;            // 0.1875 is the conversion for bits to voltage, may need to change
  
  
 
  Serial.print("AIN0: "); Serial.print(IR_925);
  Serial.print("\tLED1 Voltage: "); Serial.println(IR_925_voltage, 7);
  
  Serial.print("AIN1: "); Serial.print(IR_880);
  Serial.print("\tLED2 Voltage: "); Serial.println(IR_880_voltage, 7);
  
  Serial.print("UV-B: "); Serial.print(GUVA);
  Serial.print("\tUV-B Voltage: "); Serial.println(GUVA_voltage, 7);
  
  Serial.print("UV-A: "); Serial.print(UV_A);
  Serial.print("\tUV-A Voltage: "); Serial.println(UVA_voltage, 7);
  Serial.println(" ");
  delay(1000);   // amount of time between samples
   
  //-----------------------------------------------------------------------------------------------------------------


 
  //-----------------------------------------------------------------------------------------------------------------
  
  //-----------------------------------------------------------------------------------------------------------------
} 
