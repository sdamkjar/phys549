// ~~~~~ HAB Datalogger Sketch Details~~~~~
/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Robson
 * Created: July 17, 2017
 * 
 ***************************************************************************************************/

// ~~~~~ List of improvements ~~~~~
/***************************************************************************************************
 *  1. Make a loop to populate the zeros in the data array instead of writing it all out individually.
 *  2. Write directly to the data 2D array instead of making a 1D array and writing the 1D array to the 2D array.
 *  3.
 */
//========================================================================
// ~~~~~ LIBRARIES ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_ADS1015.h>   
#include <Adafruit_VEML6070.h>
#include <utility/imumaths.h>
#include <Adafruit_GPS.h>

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~


// User Changeable Variables..............................................

uint32_t data_time=100; // sample rate to read data to timer
uint32_t LoRa_mode_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000; // Time between SD write
uint32_t LoRa_push_time=5000; // Time between LoRa push
uint32_t cutoffAlt = 3000; //Altitude limit
uint32_t burn_time = 15000; //How long to leave the relay active
int LoRaMode = 1;             // --> either 1, 2, or 3

// set timers prior to running the loop...................................
uint32_t state1timer = millis(); //on-board clock
uint32_t state2timer = millis(); //GPS time
uint32_t state3timer = millis(); //on-board clock
uint32_t state4timer = millis(); //on-board clock
uint32_t relayTest_time=millis(); //timer to test the relay

// GPS Variable definition................................................

#define mySerial Serial1 //Change the name of Serial1 (the UART) to mySerial
Adafruit_GPS GPS(&mySerial);
#define GPSECHO false /* Set to 'false' to turn off gps echo to serial, and to true if it should echo to serial */
boolean usingInterrupt = false; // this keeps track of whether we're using the interrupt
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy
uint8_t Lon=0;
uint8_t Lat=0;
uint8_t Alt=0;

// SD Card ..............................................................
const int chipSelect = 4; // Selecting chip for the SD Card
File dataFile; // Name of the datafile stored on the SD card 

//Adafruit ID assigning..................................................

Adafruit_BNO055 bno = Adafruit_BNO055(55); // Assign a unique ID to the BNO055
Adafruit_ADS1115 ads1115; //Assign a unique ID to the ADC
Adafruit_VEML6070 uv = Adafruit_VEML6070();   //Assign a unique ID to the VEML6070

// Pin selection stuff...................................................

const byte shutdownPin=14;
const byte relayPin = 18;
const byte GeigerPin[3] = {15, 16, 17}; // Set interrupt pin locations for Geiger counters --> pin17 is AND gate

// Data Collection Variables.............................................
int row_count=0; //Used for data collection For loop
float data[20][20] ={ }; //Used to store data prior to writing to SD
int16_t GUVA, UV_A; //Assigning photometry sensor variables.
uint32_t gCounter[3];             // ISR = Geiger counter logs cosmic ray events--> gCounter[3] = AND output 
uint32_t gCounts[3];              // --> used to log data at state1rate increments

//Relay Variables........................................................
int relayBurned = 0; //Relay burn counter
int delayRelay = 0;

// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{
  Serial.begin(115200);   //Set USB serial speed
  
  Serial.print("HAB Balloon Data Test"); Serial.print("");

  //LoRa Mode Setup......................................................

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

  pinMode(GeigerPin[0], INPUT);
  pinMode(GeigerPin[1], INPUT);
  pinMode(GeigerPin[2], INPUT);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[0]), ISR_GC1, FALLING); 
  attachInterrupt(digitalPinToInterrupt(GeigerPin[1]), ISR_GC2, FALLING);
  attachInterrupt(digitalPinToInterrupt(GeigerPin[2]), ISR_GC3, FALLING);
  
  //Cutdown Relay Setup...................................................
  
  pinMode(relayPin,OUTPUT);
  
  //Shutdown Setup........................................................
  
  pinMode(shutdownPin,INPUT); 

  //GPS Setup.............................................................
  // Setting the speed of the GPS serial bus
  
  GPS.begin(115200);
  mySerial.begin(115200);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // Set the 1 Hz update rate
  GPS.sendCommand(PGCMD_ANTENNA); // Request updates on antenna status, comment out to keep quiet

  //May be unnecessary, don't know what it does.
  #ifdef __arm__
  usingInterrupt = false;  //NOTE - we don't want to use interrupts on the Due
  #else
  useInterrupt(true);
  #endif

  delay(1000); //Think it allows 1 update from the GPS first.
  
  //SD CARD SETUP.........................................................

  Serial.print("Initializing SD card...");
  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(SS, OUTPUT);
  
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1) ;
  }
  Serial.println("card initialized.");

    // create a new file
  char filename[] = "LOGGER00.txt";
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i/10 + '0';
    filename[7] = i%10 + '0';
    if (! SD.exists(filename)) {
      // only open a new file if it doesn't exist
      dataFile = SD.open(filename, FILE_WRITE); 
      break;  // leave the loop!
    }
  }

  char dataheader = "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2";
  dataFile.println(dataheader);
  
  //BNO Setup.........................................................
  if(!bno.begin())  //Initialise the BNO sensor
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }    
  bno.setExtCrystalUse(true);

  //ADC (ADS115) Setup.........................................................
  ads1115.begin();

  //Serial UV Setup.........................................................
  uv.begin(VEML6070_1_T);  // pass in the integration time constant

  
  //Pre-Flight Readouts.................................................................
  Serial.println(filename);
  Serial.println(dataheader);
  /* Add in pre-flight readouts such as cut-off altitude etc.*/

  

}

//==================================================================================================
//Don't know if this is useful, 
#ifdef __AVR__
// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
#ifdef UDR0
  if (GPSECHO)
    if (c) UDR0 = c;  
    // writing direct to UDR0 is much much faster than Serial.print 
    // but only one character can be written at a time. 
#endif
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }



}

#endif //#ifdef__AVR__

//==================================================================================================


// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity

  //State 1 Timer Reset.....................................................
  if (millis() - state1timer > data_time ) { 
    state1timer = millis(); // reset the timer
  
    //GPS Data Collection.....................................................
  
    // Maybe purge this whole thing PURGE IT ALL
    
    // in case you are not using the interrupt above, you'll
    // need to 'hand query' the GPS, not suggested :(
    if (! usingInterrupt) {
      // read data from the GPS in the 'main loop'
      char c = GPS.read();
      // if you want to debug, this is a good time to do it!
      if (GPSECHO)
        if (c) Serial.print(c);
  
    }
  
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }
    
    //Loop to continue logging GPS data regardless of if there is a fix or not.
    if (GPS.fix) 
    {
      Lon=GPS.lon;
      Lat=GPS.lat;
      Alt=GPS.altitude;
    }
    else
    {
      Lon=0;
      Lat=0;
      Alt=0;
    }
    
    //Geiger Data Collection.....................................................

    for (int i = 0; i < 3; i++) {
      gCounts[i] = gCounter[i];     // Log separate Geiger counter events in 3elem array
      gCounter[i] = 0;              // Reset Geiger interrupt counters
    }
    
    //BNO Data Collection........................................................
    
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER); 
    //Getting Euler variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()
    
    imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); 
    //Getting Rotation Rate variables in 3 element structure/object directly from the IMU on the BNO, is called as euler.x() or euler.y()

    //LED Data Collection........................................................
    delay(2);                                       //Delay necessary for the ADC, for any other ADC terminals, require a 2 millisecond offset between sampling.
    GUVA = ads1115.readADC_SingleEnded(2);          // digital representation of the voltage from the GUVA (analog UV sensor)
    UV_A = uv.readUV();                             // digital representation of the voltage from the UVA
 

    //Data collection for print..................................................
    // String header: "Clock_Time, GPS Hour, GPS Minute, GPS Seconds, GPS Milliseconds, FIX, LAT, LONG, ALT, E_X, E_Y, E_Z, G_X, G_Y, G_Z,  GUVA, UV_A, gCounts0, gCounts1, gCount2";

    //Assign collected data to current data array
    float currentdata[1][20]={
      {millis(),GPS.hour,GPS.minute,GPS.seconds,GPS.milliseconds,GPS.fix,GPS.lat,GPS.lon,GPS.altitude,euler.x(),euler.y(),euler.z(),gyro.x(),gyro.y(),gyro.z(), GUVA, UV_A,gCounts[0],gCounts[1],gCounts[2],}
    };

    //This loop rights from current data into the data array.
    for (int j =0; j < 20; j++) {
      data[row_count][j]=currentdata[0][j];
    }
    row_count++; //Increase counter by one to write to the next row of the 2D data array
 
  }

  // ~~~~~ BEGIN STATE 2: LORA MODE SWITCHING ~~~~~
  //========================================================================
  /*
  // State 2: switch LoRa modes (CG)................................................
  if ( (GPStime() - state2time) > 2min ) {
    switch(LoRaMode) {
      case 1: {
        //switch to case 2
        LoRaMode = 2;
        }
        break;
      case 2: {
        //switch to case 3
        }
        LoRaMode = 3;
        break;
      case 3: {
        //switch to case 1
        LoRaMode = 1;
        }   
        break;
     }
  }
  */

  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)


  //State 3 timer reset.....................................................
  if ( (millis() - state3timer) > SD_time ) {
    state3timer = millis(); //Reset the timer.
  
    //SD card write loop.....................................................
    
    //This loop rights data collected in the dataFile array to serial and to SD card.
      for (int Y =0; Y < round(SD_time/data_time); Y++) {
      Serial.println();
      dataFile.println();
        for (int X =0; X < 20; X++) {
          Serial.print(data[Y][X]);
          dataFile.print(data[Y][X]);
      
          if (X<19) {
            Serial.print(",");
            dataFile.print(",");
          }
      
         }
  
      }
    //Flushing data and reseting counter......................................
    dataFile.flush(); //Flush data and write to SD card
    row_count=0; //Reset the 2D array counter to 0.

    //SD Card Shutdown Loop...................................................
    //Loop to close the SD card if the shut off pin has been disconnected.
    if (digitalRead(shutdownPin) == HIGH){
      dataFile.close();
    }

    
  }

  
  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION

  // State 4 timer..........................................................
  if ( (millis() - state4timer) > LoRa_push_time ) {
    // push desired data packet to LoRa
    state4timer = millis();
    
  // Fill in after this....................................................

  }


  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here
  
  // Cutoff relay altitude check...........................................
  if ( Alt > cutoffAlt && relayBurned == 0) {
    // set pin high to activate relay
      digitalWrite(relayPin, HIGH); 
      delayRelay = millis();
      relayBurned = 1;
  }
  // Relay shutoff...........................................
  // Relay switch to low voltage - turns the COM to NC
  else if (millis() - delayRelay > burn_time) {
    digitalWrite(relayPin, LOW); 
  }
  
}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

void ISR_GC1() {
  gCounter[0] = gCounter[0] + 1;
}

void ISR_GC2() {
  gCounter[1] = gCounter[1] + 1;
}

void ISR_GC3() {
  gCounter[2] = gCounter[2] + 1;
}


//=================================================================================

