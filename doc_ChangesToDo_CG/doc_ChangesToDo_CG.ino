/***************************************************************************************************
 * Comment document for changes required in CR_HAB_Datalogger_Example_Sketch1
 * 
 * Author: Caelia Gardiner/Chris Robson
 * Created: July 25, 16:15
 * 
 ***************************************************************************************************/

//1 Check relay loop software logic, specifically "relayBurned == 0" check

//3 Serial print lines removed for flight software? -> no, make more read outs for flight

//4 GPS code purging may be possible

//6 Look at different methods for storing data on RAM before SD write

//7 Check delay(2ms) from ADC with state machine timing

//8 Test shut down pin procedure with SD close file 

//9 Add LED blink for proper operation of state machine

//10 Add LED blink for proper GPS fix and for ensuring closed SD file


