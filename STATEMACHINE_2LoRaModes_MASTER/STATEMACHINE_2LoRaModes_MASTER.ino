/***************************************************************************************************
 * Description: 
 * 
 * 
 * Author: C. Gardiner
 * Created: July 26, 2017
 * 
 ***************************************************************************************************/

//========================================================================
// ~~~~~ Libraries required ~~~~~

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <RH_RF95.h> // Library for LoRa radios, from RadioHead

//========================================================================
// ~~~~~ Loop Global Variable Definitions ~~~~~

#define DEBUG 1

// User Changeable Variables..............................................

uint32_t data_time=100;         // sample rate to read data to timer
uint32_t RH_RF95_LORA_MODE_time=120000; // Time between LoRa mode change
uint32_t SD_time=1000;          // Time between SD write
uint32_t LoRa_push_time=5000;   // Time between LoRa push
uint32_t cutoffAlt = 3000;      // Altitude limit
uint32_t burn_time = 15000;     // How long to leave the relay active
int LoRaMode = 1;               // --> either 1, 2, or 3

// set timers prior to running the loop...................................
uint32_t state1timer = millis();  //on-board clock
uint32_t state2timer = millis();  //GPS time
uint32_t state3timer = millis();  //on-board clock
uint32_t state4timer = millis();  //on-board clock
uint32_t relayTest_time=millis(); //timer to test the relay

// GPS Variable definition................................................


// SD Card ..............................................................


// Adafruit ID assigning..................................................


// Pin selection stuff...................................................


// Data Collection Variables.............................................


// Relay Variables........................................................


// LoRa TX Variable Definition...............................................

// Pulse width on reset assert (msec)
#define RH_RF95_RESET_DELAY 10
// Must match frequency of target (MHz)
#define RF95_FREQ           915.0
// Packet length (RH_RF95_MAX_MESSAGE_LEN = 255 - RH_RF95_HEADER_LEN)
#define RADIO_PACKET_LEN    20 // why???
// Transmit attempt timeout (msec)
#define TX_TIMEOUT          1000
// Time between sending packets (msec)
#define TX_PERIOD           5000
// Pin assignments for LoRa SPI interface
#define RFM95_CS            10
#define RFM95_INT           6
#define RFM95_RST           9
#define LED 8

uint8_t lora_mode = RH_RF95_LORA_MODE_DEFAULT;
bool    loraForceReset = false;
bool    loraInitSuccess = false;

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

// Prototype of lora reset function
int lora_init(uint8_t newMode);

char radiopacket[RADIO_PACKET_LEN] = "Hello World #      "; // Does it matter that this is defined as a char if we plan to assign it as an array?


// ~~~~~ SETUP ~~~~~~
//========================================================================
  
void setup() 
{

#ifdef DEBUG
  Serial.begin(115200);
  // Delay until serial interface is initialized
  while (!Serial) delay(1);
#endif

  //LoRa TX Mode Setup......................................................
  pinMode(RFM95_RST, OUTPUT);




  

  //Status LED Setup......................................................

  
  //Geiger Counts Setup...................................................

 
  //Cutdown Relay Setup...................................................
  
  
  //Shutdown Setup........................................................
  

  //GPS Setup.............................................................

  
  //SD CARD SETUP.........................................................

  
  //BNO Setup.........................................................


  //ADC (ADS115) Setup.........................................................


  //Serial UV Setup.........................................................

  
  //Pre-Flight Readouts.................................................................


}

// LoRa TX code outside of setup and loop..................................................
int16_t packetnum = 0;  // packet counter, we increment per xmission

// ~~~~~ LOOP ~~~~~~
//========================================================================
void loop() {

  // ~~~~~ BEGIN STATE 1: DATA COLLECTION ~~~~~
  //========================================================================

  //State 1 Description.....................................................
    // collect data into vectored variables for the following:
    //    GPS position, time
    //    on-board clock time - millis()
    //    photometry voltages
    //    Geiger counter events
    //    BNO Euler angles, rotational velocity
    //    radiopacket array

    // LoRa radiopacket array
    //String radiopacket[RADIO_PACKET_LEN] = "FRQ," + String(RF95_FREQ) + ",T," + String(currentdata[1]) + ":" + String(currentdata[2]) + ":" + String(currentdata[3]) + "." + String(currentdata[4]) + ",FX" + "," + String(currentdata[5]) + ",LT," + String(currentdata[6]) + ",LG," + String(currentdata[7]) + ",ALT," + String(currentdata[8]) + ",GUVA," + String(currentdata[9]) + ",UV_A," + String(currentdata[10]) + ",GC0," + String(currentdata[11]) + ",GC1," + String(currentdata[12]) + ",GC2," + String(currentdata[13]) + ",BRN," + String(currentdata[14]); //radio packet string 








  // ~~~~~ BEGIN STATE 2: LORA TX MODE SWITCHING ~~~~~
  //========================================================================

  //State 2 Description.....................................................

//if (GPS.minute >= 0 & GPS.minute < 3)
//  {
//    lora_mode = 1; // set LoRa mode 1 when minutes is 0<=t<3
//  }
//else if (GPS.minute >= 3 & GPS.minute < 6)
//  {
//    lora_mode = 2; // set LoRa mode 1 when minutes is 3<=t<6
//  }
//else if (GPS.minute >= 6 & GPS.minute <= 9)
//  {
//    lora_mode = 3; // set LoRa mode 1 when minutes is 3<=t<6
//  }



  // ~~~~~ BEGIN STATE 3: SD WRITE ~~~~~
  //========================================================================

  // State 3 Description....................................................
    // Writing to SD Card BLAH BLAH (fill in later)







  // ~~~~~ BEGIN STATE 4: LORA PUSH ~~~~~
  //========================================================================
  // State 4 Description....................................................
      //FILL IN DESCRIPTION

static unsigned long lastTxTime = 0; 


if( lora_init(lora_mode) == RH_RF95_ERR_NO_CHANGE)
{
  if (millis() - lastTxTime > TX_PERIOD)
  {

  #ifdef DEBUG
    Serial.print("\n\rTransmitting..."); // Send a message to rf95_server
  #endif
    
    lastTxTime = millis();
    
    if(!loraInitSuccess)
     loraForceReset = true;

    if(!rf95.send((uint8_t *)radiopacket, RADIO_PACKET_LEN, TX_TIMEOUT))
     loraForceReset = true; 
  }
}



  // ~~~~~ BEGIN STATE 5: CUT DOWN RELAY ~~~~~
  //========================================================================
  
  // State 5 description...................................................
    //Fill in description here
  




}

// ~~~~~ ISR FOR GEIGER COUNTER EVENTS ~~~~~
//================================================================================

  // ISR description...................................................
    //Fill in description here




//=================================================================================

// ~~~~~ LoRa Mode Switching Subfunction ~~~~~
//================================================================================

  // LoRa Mode Switching Subfunction description...................................................
    //Fill in description here
int lora_init(uint8_t newMode){

    static uint8_t          mode      = RH_RF95_LORA_MODE_INITIAL;
    static unsigned long    resetWait = 0;
    static bool             resetting = false;

    if (loraForceReset)
    {
    #ifdef DEBUG
        Serial.print("\n\rLoRa not responding! Attempting reset...");
    #endif
      loraForceReset = false;
      loraInitSuccess = false;
    }
    else if (mode == newMode)
    {
        return RH_RF95_ERR_NO_CHANGE;
    }


    if (!resetting)
    {
        digitalWrite(RFM95_RST, LOW);
        resetWait = millis();
    }

    resetting = true;

    if (millis() - resetWait < RH_RF95_RESET_DELAY)
    {
        return RH_RF95_ERR_RESET_WAIT;
    }

    digitalWrite(RFM95_RST, HIGH);

    resetting = false;

    if (!rf95.init())
    {
    #ifdef DEBUG
        Serial.print("\n\rERROR! LoRa radio init failed.");
    #endif
        return RH_RF95_ERR_LORA_INIT;
    }

    if (!rf95.setFrequency(RF95_FREQ)) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! setFrequency failed.");
    #endif
        return RH_RF95_ERR_SET_FREQ;
    }

    rf95.setTxPower(20, false);

    rf95.sleep();

    if (rf95.setOPModeLoRa() != RH_RF95_ERR_NO_ERROR) {
    #ifdef DEBUG
        Serial.print("\n\rERROR! Failed to enter LoRa Mode.");
    #endif
        return RH_RF95_ERR_LORA_MODE;
    }

    rf95.setModeIdle();

    switch(LoRaMode){
    case RH_RF95_LORA_MODE_DEFAULT:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    case RH_RF95_LORA_MODE_1:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_5);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_4096CPS);
    break;
    case RH_RF95_LORA_MODE_2:
      rf95.setBandWidth(RH_RF95_BW_250KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_7);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_1024CPS);
    break;
    case RH_RF95_LORA_MODE_3:
      rf95.setBandWidth(RH_RF95_BW_125KHZ);
      rf95.setCodingRate(RH_RF95_CODING_RATE_4_6);
      rf95.setSpreadingFactor(RH_RF95_SPREADING_FACTOR_128CPS);
    break;
    }

    rf95.readRegModemConfig();

    0x28 & 0x0F = 0x08

    mode = newMode;

    loraInitSuccess = true;

    return RH_RF95_ERR_NO_ERROR;

}

GPSminute % 10 >=0 && GPSminute % 10 < 3

